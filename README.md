# HTMProject

## What is an HTM?

Hierarchical Temporal Memory (HTM) replicates the structural and algorithmic
properties of the neocortex. It can be regarded as a memory system which is not
programmed, but trained through exposing it to data flow. The process of training is
similar to the way humans learn which, in its essence, is about finding latent causes
in the acquired content.


The detailed presentation of an HTM can be found in [Numenta whitepaper](http://numenta.org/resources/HTM_CorticalLearningAlgorithms.pdf).


## What this project is for?

Creating easy to understand and modify object-oriented implementation of the algorithm, with computationaly demending and heavely parallel fragments ported to [OpenCL](https://www.khronos.org/opencl/) (using [PyOpenCL](https://documen.tician.de/pyopencl/)).

## How to install?

Tested on Ubuntu 14.04 and Mac OS X 10.11.

1. Clone repository
2. Install fortran compiller, blas, lapack, png & freetype libs, as well as python headers eg.: `sudo apt-get install gfortran libopenblas-dev liblapack-dev libpng-dev libfreetype6-dev python-dev`
3. Install [OpenCV](http://opencv.org/)
4. Install [OpenCL](https://wiki.tiker.net/OpenCLHowTo#How_to_set_up_OpenCL_in_Linux) if needed. Warning: since this may involve changing GPU drivers, be very careful.
5. Install [PyOpenCL](https://wiki.tiker.net/PyOpenCL/Installation)
6. Run `pip install -r requirements.txt`
7. Run `python setup.py develop` (for development) or `python setup.py install` (for usage)
8. To use pycallgraph profiler install Graphviz (`sudo apt-get install graphviz`)

## Running HTM

Basic modes documentation is available by invoking:

    htmproject -h
    
Each mode has additional help available (including arguments descriptions):

    htmproject {learn,test,stats,recover,generate} -h
    

### Single run

HTM can be run in two modes: learning and testing. They accept different sets of arguments (though some of them are common).

#### Learning mode

The simplest version of HTM running in learning mode looks like this:

    htmproject learn path/to/config.json
    
One of the most useful options is `--o-pickle` - path to file to which trained HTM will be saved (ie. to be later used for testing). Another is `--c-pickle` - path for saving classifier (LinearSVM in current setup) pickle. Please note that those params do not have default values - if they are not provided, no pickles will be saved.

For working config examples look into `test_data/functional_cli` dir.
    
#### Testing mode

Used to gather histograms in JSON file, as well as (optionally) classifier stats. Basic command:

    htmproject test path/to/config.json --i-pickle=path/to/htm_modules.pickle --c-pickle=path/to/classifier.pickle
    
Please note that `--i-pickle` (path to trained HTM pickle) argument is required.

### Statistics

#### Gathering

To run HTM in learning mode with the purpose of statistics gathering (while changing single param value within specified range and with particular step), use:

    htmproject stats -c path/to/config_template.json -s path/to/statistics_config.json
    
`core.param.name` is created by traversing the configuration tree. For example, to see how changing amount of 0-layer columns influences F1-score, desired parameter will be `core.htm.columns_per_layer.0`. To see how spatial pooler winners set size changes results, use `core.sp.winners_set_size`. To see how different values of temporal pooler permanence value increase affect learning results, use `core.tp.perm_inc`.

For working config examples look into `test_data/stats` dir.

#### Recovery

If for some reason stats gathering was interrupted, partial results recovery may be possible with:

    htmproject recover path/to/test/dir

#### Generation (dry-run)

If only test sets/test configs generation is needed (no actual stats run) use:

    htmproject generate -c path/to/config_template.json -s path/to/statistics_config.json