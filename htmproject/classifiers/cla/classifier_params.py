params = {
    # Prediction step
    # Number or list
    # List:
    #   [1 4 6] - make prediction for step 1, 4 and 6 in future
    # Number:
    #   5 = [1 2 3 4 5]
    'p_step': 10,
    # Input size (number of cells in TP)
    'input_size': 500,
    # Number of classes
    'class_number': 10
}
