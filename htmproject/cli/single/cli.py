from htmproject.interfaces.cli import CLI


class SingleCLI(CLI):
    def __init__(self, *args, **kwargs):
        super(SingleCLI, self).__init__(*args, **kwargs)

        self._logger.info('CLI SETUP: Single HTM')

    def _get_module_names(self):
        return ['default']

    def _combine_results(self, testing_results):
        self._logger.info('Unwrapping results...')

        combined_results = []
        for chunk_name, item in testing_results:
            combined_results.append((chunk_name, {
                'class_name': item['class_name'],
                'histogram': item['histograms']['default']
            }))

        self._logger.info('Results unwrapped')

        return combined_results
