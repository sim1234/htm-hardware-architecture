class ColumnState(object):
    """

    """

    def __init__(self):
        """

        :return:
        """
        self.active = False

    def is_active(self):
        """

        :rtype: bool
        :return:
        """
        return self.active

    def set_active(self, state):
        """

        :param state: bool
        """
        self.active = state
