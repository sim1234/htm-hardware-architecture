from .spatial_pooler_basic import SpatialPoolerBasic

try:
    from .spatial_pooler_gpu import SpatialPoolerGPU
except ImportError:
    pass
