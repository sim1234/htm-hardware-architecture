from htmproject.core.object_oriented.column_connector import ColumnConnector
import numpy


class ColumnConnectorVectorized(ColumnConnector):
    __slots__ = ('_config', '_column', '_input_length', '_columns_amount', '_overlap_history_buffer_length',
                 '_active_history_buffer_length', '_boost', '_inhibition_radius', '_number_of_synapses',
                 '_active_after_inhibition', '_overlap', '_min_overlap', '_max_duty_cycle', '_min_duty_cycle',
                 '_overlap_duty_history', '_active_duty_history', '_overlap_duty_cycle', '_active_duty_cycle',
                 '__init_perm', '__perm_inc', '__perm_dec', '__connected_perm', '__perm_boost',
                 '__existing_connections', '__perm_values')

    def __init__(self, layer, config, *args, **kwargs):
        self.__init_perm = float(config.get('core.sp.init_perm'))
        self.__perm_inc = float(config.get('core.sp.perm_inc'))
        self.__perm_dec = float(config.get('core.sp.perm_dec'))
        self.__connected_perm = float(config.get('core.sp.connected_perm'))
        self.__perm_boost = 1 + 0.1 * self.__connected_perm

        super(ColumnConnectorVectorized, self).__init__(layer, config, *args, **kwargs)

    def _generate_connections(self):
        self.__existing_connections = numpy.nonzero(self._get_connected_indexes())
        self.__perm_values = numpy.random.normal(
            loc=self.__init_perm,
            scale=0.025,
            size=(self._number_of_synapses, )
        )

    def __get_zero_activity(self):
        return numpy.zeros((self._number_of_synapses,), dtype=bool)

    def boost_perm_values(self):
        if self._overlap_duty_cycle <= self._min_duty_cycle:
            # noinspection PyAttributeOutsideInit
            self.__perm_values = numpy.minimum(self.__perm_values * self.__perm_boost, 1)

    def update_inhibition_radius(self):
        connected_synapses = numpy.nonzero(self.__perm_values >= self.__connected_perm)[0]

        if connected_synapses.shape[0]:
            reception_field = connected_synapses[-1] - connected_synapses[0]
            self._inhibition_radius = max(int(round(
                (self._columns_amount * reception_field) / (3 * self._number_of_synapses)
            )), 1)

    def update_synapses(self, input_item):
        received_input = input_item[self.__existing_connections]
        to_increase = numpy.nonzero(received_input)
        to_decrease = numpy.nonzero(1 - received_input)

        self.__perm_values[to_increase] = numpy.minimum(self.__perm_values[to_increase] + self.__perm_inc, 1)
        self.__perm_values[to_decrease] = numpy.maximum(self.__perm_values[to_decrease] - self.__perm_dec, 0)

    def calculate_overlap(self, input_item):
        self._overlap = numpy.count_nonzero(self.get_synapses_activity(input_item))

    def get_synapses_activity(self, input_item):
        received_input = input_item[self.__existing_connections]

        return numpy.logical_and(received_input, self.__perm_values >= self.__connected_perm)
