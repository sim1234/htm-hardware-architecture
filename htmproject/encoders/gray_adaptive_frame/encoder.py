import numpy
from htmproject.encoders.adaptive_frame import AdaptiveFrameEncoder
from skimage.filters import threshold_otsu


class GrayAdaptiveFrameEncoder(AdaptiveFrameEncoder):

    def _convert_to_binary(self, frame):
        try:
            thresh = threshold_otsu(frame)
        except TypeError:
            thresh = numpy.full_like(frame, 0.5)
        binary = frame > thresh

        frame = binary.flatten()
        return frame
