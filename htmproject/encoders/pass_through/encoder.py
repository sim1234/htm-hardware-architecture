from htmproject.interfaces.encoder import Encoder


class PassThroughEncoder(Encoder):
    """
    Maps from the "normal" space to the multidimensional binary space
    """
    def __init__(self, config):
        """
        :param config: Configuration object
        """
        self.__width = config.get("encoder.width")

    def get_item_size(self):
        """

        :return: size of the binary space
        :rtype: int
        """

        return self.__width

    def encode(self, item):
        """
        Maps item to SDR.
        :param numpy.array item:
        :return: SDR of item
        :rtype: numpy.array
        """

        return item > 0  # change input to bool
