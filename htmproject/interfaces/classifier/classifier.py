from sklearn import metrics


class Classifier(object):
    """
    Classifies output histograms to one of the predefined categories.
    """

    def __init__(self, config):
        self._config = config

    def train(self, train_data, target):
        """
        The method must be overloaded in the child class and model fit must be provided (e.g.
        self.model.fit(train_data, target))

        :param numpy.ndarray train_data: Data to be used for training.
        :param numpy.ndarray target: Categories (e.g. [0, 0, 0, 1, 1, 2, 1, 0]); number of different values
            determines amount of classes
        """
        raise NotImplementedError()

    def classify(self, data):
        """
        Classifies result histograms to one of the categories.
        :param numpy.ndarray data: histograms to be classified
        :rtype: numpy.ndarray
        :return: Categories the items in data belong to.
        """
        raise NotImplementedError()

    def get_classification_stats(self, data, target, labels):
        """
        :rtype: dict
        :return: Classification statistics
        """
        results = self.classify(data)

        f1_score = metrics.f1_score(
            target,
            results,
            average='weighted',
            pos_label=None
        )
        confusion_matrix = metrics.confusion_matrix(
            target,
            results,
            labels=sorted(dict(enumerate(labels)), key=labels.__getitem__)
        )
        return {
            'f1_score': f1_score,
            'confusion_matrix': confusion_matrix,
            'labels': sorted(labels)
        }

