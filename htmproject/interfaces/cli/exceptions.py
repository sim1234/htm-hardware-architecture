class TooManyIterationsError(Exception):
    pass


class InputError(Exception):
    pass


class ConfigError(Exception):
    pass

class PickleError(Exception):
    pass
