import sys

try:
   import cPickle as pickle
except:
   import pickle

sys.setrecursionlimit(100000)


class Pickler(object):
    @staticmethod
    def load(filename):
        with open(filename, 'rb') as pickle_file:
            return pickle.load(pickle_file)

    @staticmethod
    def dump(filename, obj):
        with open(filename, 'wb') as pickle_file:
            return pickle.dump(obj, pickle_file, pickle.HIGHEST_PROTOCOL)
