class Decoder(object):
    """

    """

    def decode(self, item):
        """
        Maps from SDR to the result space.
        :param numpy.array item:
        :return: item mapped from SDR
        :rtype: numpy.array
        """
        raise NotImplementedError()

