class Encoder(object):
    """

    """

    def get_item_size(self):
        """
        :return: size of the binary space
        :rtype: int
        """
        raise NotImplementedError()

    def encode(self, item):
        """
        Maps item to SDR.
        :param numpy.array item:
        :return: SDR of item
        :rtype: numpy.array
        """
        raise NotImplementedError()
