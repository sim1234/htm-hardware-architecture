class ChunkReader(object):
    def next_chunk(self):
        raise NotImplementedError()

    def reset_chunk(self):
        raise NotImplementedError()
