import logging
import struct
from io import BytesIO

import skimage.io

from htmproject.interfaces.iostreamer.chunk.reader import ChunkReader
from htmproject.interfaces.iostreamer.protobuf.utils import setup_from_config
from htmproject.interfaces.iostreamer.reader import Reader


class ProtoBufReader(Reader, ChunkReader):
    """
    ProtoBuf reader class
    """

    def __init__(self, config):
        self._config = config
        self.proto, self.extras = setup_from_config(self._config)

        self._dataset_msg_len = 0
        self._dataset = None

        self._file = None
        self._items = None

        self._current_group = None
        self._groups = self._read_groups()

        self._filter = None
        self._logger = logging.getLogger('htm')

    def get_next_data_item(self):
        """

        :return: single datum as an n-dimensional matrix of integers
        :rtype: numpy.ndarray
        """
        return self._items.next()

    def open(self):
        self._dataset = self.proto.Dataset()
        self._file = open(self._config.get('iostreamer.data_file'), "rb")

        self._dataset_msg_len = struct.unpack('>L', self._file.read(4))[0]
        self._dataset.ParseFromString(self._file.read(self._dataset_msg_len))

    def close(self):
        self._file.close()

    def get_data_set_size(self):
        if not self._dataset:
            self.open()
            self.close()
        return self._dataset.items

    def _read_items(self):
        """

        :return:
        """
        for item in self._current_group.items:
            buf = BytesIO(bytearray(item.value))
            frame = skimage.io.imread(buf, plugin='pil')
            yield frame
        raise StopIteration()

    def _read_groups(self):
        while True:
            read_bytes = self._file.read(4)
            if not len(read_bytes):
                raise StopIteration()
            group_msg_len = struct.unpack('>L', read_bytes)[0]
            group = self.proto.Group()
            group.ParseFromString(self._file.read(group_msg_len))
            yield group

    def next_chunk(self):
        """
        :rtype: dict|None
        :return: Chunk (file) name, label, and size (frames count) or None if no more files available
        """
        try:
            group_candidate = self._groups.next()

            if self._filter is not None:
                while group_candidate.label != self._filter:
                    group_candidate = self._groups.next()

            self._current_group = group_candidate
            self._items = self._read_items()

            return {
                'name': self._current_group.name,
                'label': self._current_group.label,
                'size': self._current_group.size
            }
        except StopIteration:
            return None

    def reset_chunk(self):
        self._items = self._read_items()

    def reset(self):
        self.close()
        self.open()
        self._groups = self._read_groups()

    def set_class_filter(self, name):
        self._filter = None
        if name is not None:
            filtered = [k for k,v in self._dataset.labels.iteritems() if v == name]
            if len(filtered):
                self._filter = filtered[0]
            else:
                self._logger.warning('Could not find class {} in dataset'.format(name, self._filter))

        self._logger.debug('Reader class filter set to {} ({})'.format(name, self._filter))

    def get_name(self):
        return '{}--{}'.format(self._current_group.label, self._current_group.name)

    def get_labels(self):
        return [v for k, v in sorted(self._dataset.labels.items(), key=lambda x: x[0])]
