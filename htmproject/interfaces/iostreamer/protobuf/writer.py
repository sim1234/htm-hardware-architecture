import struct

from htmproject.interfaces.iostreamer.protobuf.utils import setup_from_config
from htmproject.interfaces.iostreamer.writer import Writer


class ProtoBufWriter(Writer):
    """
    ProtoBuf writer class
    """

    def __init__(self, config):
        self._config = config
        self.proto, self.extras = setup_from_config(self._config)

        self.file = None

    def save_data_item(self, item):
        """
        :param item: numpy.ndarray
        """
        item_msg = self.proto.Item()
        item_msg.value[:] = item.tobytes()
        self.save_message(item_msg)

    def open(self):
        self.file = open(self._config.get('iostreamer.data_file'), 'wb')

    def close(self):
        self.file.close()

    def save_message(self, item_msg):
        item_msg_len = struct.pack('>L', item_msg.ByteSize())
        self.file.write(item_msg_len)
        self.file.write(item_msg.SerializeToString())
