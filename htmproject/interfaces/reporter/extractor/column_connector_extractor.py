from operator import itemgetter
from .extractor import Extractor
from .sp_synapse_extractor import SPSynapseExtractor


class ColumnConnectorExtractor(Extractor):
    @staticmethod
    def extract(connector, input_item):
        synapse_extractor = SPSynapseExtractor()
        bool_to_int = lambda x: 1 if x else 0

        return {
            'synapses': sorted([
                synapse_extractor.extract(synapse, input_item) for synapse in connector.get_synapses()
            ], key=itemgetter('input_index')),

            'max_duty_cycle': connector.get_max_duty_cycle(),
            'min_duty_cycle': connector.get_min_duty_cycle(),
            'overlap_duty_history': map(bool_to_int, connector.get_overlap_duty_history()),
            'active_duty_history': map(bool_to_int, connector.get_active_duty_history()),
            'overlap_duty_cycle': connector.get_overlap_duty_cycle(),
            'active_duty_cycle': connector.get_active_duty_cycle(),
            'overlap': connector.get_overlap()
        }
