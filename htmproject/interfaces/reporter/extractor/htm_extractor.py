from .extractor import Extractor
from .layer_extractor import LayerExtractor


class HTMExtractor(Extractor):
    @staticmethod
    def extract(htm, input_item):
        layer_extractor = LayerExtractor()

        layers = []
        for layer in htm.get_layers():
            layers.append(layer_extractor.extract(layer, input_item))
            input_item = htm.get_tp().get_layer_output(layer)

        return {
            'layers': layers
        }
