from .extractor import Extractor
from .tp_synapse_extractor import TPSynapseExtractor


class SegmentExtractor(Extractor):
    @staticmethod
    def extract(segment):
        synapse_extractor = TPSynapseExtractor()

        data = {
            'segment_index': segment.get_segment_index(),
            'is_active': segment.is_active(),
            'is_learning': segment.is_learning(),
            'is_sequential': segment.is_sequential(),
            'synapses': [synapse_extractor.extract(synapse) for synapse in segment.get_synapses()],
            'active_synapses_count': segment.get_current_active_synapses_count(),
            'learning_synapses_count': segment.get_current_learning_synapses_count()
        }

        data['synapses_count'] = len(data['synapses'])

        return data