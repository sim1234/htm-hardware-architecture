from .extractor import Extractor


class SPStateExtractor(Extractor):
    @staticmethod
    def extract(sp_state):
        return {
            'inhibition_radius': sp_state.get_inhibition_radius()
        }