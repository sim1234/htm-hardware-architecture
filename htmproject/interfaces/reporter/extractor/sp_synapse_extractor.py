from .extractor import Extractor


class SPSynapseExtractor(Extractor):
    @staticmethod
    def extract(synapse, input_item):
        index = synapse.get_input_index()

        return {
            'input_index': index,
            'input_value': input_item[index],
            'is_active': synapse.is_active(input_item),
            'is_connected': synapse.is_connected(),
            'perm_value': synapse.get_perm_value()
        }