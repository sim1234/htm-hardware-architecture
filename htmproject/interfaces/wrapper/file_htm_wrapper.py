import copy
import os

from htmproject.interfaces.wrapper import HTMWrapper
from htmproject.mappers.dir import DirMapper

from .file_htm_wrapper_params import params as wrapper_params


class FileHTMWrapper(HTMWrapper):
    def __init__(self, *args, **kwargs):
        super(FileHTMWrapper, self).__init__(*args, **kwargs)

        self._config.add_default_params_section('wrapper', wrapper_params)

        self.__learning_dir = None
        self.__testing_dir = None

    def _get_learning_reader(self):
        return self.__get_reader(self.__learning_dir)

    def _get_testing_reader(self):
        return self.__get_reader(self.__testing_dir)

    def __get_reader(self, in_dir):
        config = copy.deepcopy(self._config)
        config['iostreamer.in_dir'] = in_dir

        return self._get_reader_class()(config)

    def get_mapper(self, classes_list=None):
        if classes_list is not None:
            return DirMapper(names=classes_list)
        else:
            files_list = self._testing_reader.get_files_list()
            return DirMapper(list_to_extract=files_list)

    def _get_reader_class(self):
        raise NotImplementedError()

    def _setup_learning(self):
        self.__learning_dir = self._config.get('wrapper.learning_dir')

        if self._class_name != 'default':
            self.__learning_dir = os.path.join(self.__learning_dir, self._class_name)

        self._logger.debug('Learning set dir: %s' % (self.__learning_dir,))

        return super(FileHTMWrapper, self)._setup_learning()

    def _setup_testing(self, testing_mode=True):
        self.__testing_dir = self._config.get('wrapper.testing_dir')
        self._logger.debug('Testing set dir: %s' % (self.__testing_dir,))

        return super(FileHTMWrapper, self)._setup_testing(testing_mode)
