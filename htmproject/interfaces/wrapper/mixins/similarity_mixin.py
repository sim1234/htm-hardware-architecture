from htmproject.interfaces.wrapper import HTMWrapper
from .similarity_mixin_params import params as wrapper_params


class SimilarityMixin(HTMWrapper):
    def __init__(self, *args, **kwargs):
        super(SimilarityMixin, self).__init__(*args, **kwargs)

        self._config.add_default_params_section('wrapper', wrapper_params)
        self.__class_similarity_threshold = float(self._config.get(
            'wrapper.class_similarity_threshold'
        ))
        self.__inter_class_similasrity_threshold = float(self._config.get(
            'wrapper.inter_class_similarity_threshold'
        ))

        self._logger.debug('Class similarity threshold: %f' % (self.__class_similarity_threshold,))
        self._logger.debug('Inter-class similarity threshold: %f' % (self.__inter_class_similarity_threshold,))

    def _has_htm_learned(self):
        needs_learning = False

        # check cosine class similarity for this HTM in testing set
        similarity = self._testing_analyzer.get_last_class_similarity()
        classes_list = similarity['class_similarity'].keys()
        used_classes = []
        for class_name in classes_list:
            used_classes.append(class_name)

            sim = similarity['class_similarity'][class_name]['cosine']
            if sim < self.__class_similarity_threshold:
                needs_learning = True
                self._logger.debug(
                    'Testing set cosine class similarity for \'%s\' is below threshold: %f' % (class_name, sim)
                )
            else:
                self._logger.debug(
                    'Testing set cosine class similarity for \'%s\': %f' % (class_name, sim)
                )

            junk_similarity = self._testing_analyzer.get_last_inter_class_similarity()
            for name, item in junk_similarity['inter_class_similarity'][class_name].items():
                if name in used_classes:
                    continue
                if item['cosine'] >= self.__inter_class_similarity_threshold:
                    needs_learning = True
                    self._logger.debug(
                        'Testing set cosine \'%s\' - \'%s\' similarity is above threshold: %f' % (
                            class_name,
                            name,
                            item['cosine']
                        )
                    )
                else:
                    self._logger.debug(
                        'Testing set cosine \'%s\' - \'%s\' similarity: %f' % (
                            class_name,
                            name,
                            item['cosine']
                        )
                    )
        return needs_learning