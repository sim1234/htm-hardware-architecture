import copy

from htmproject.interfaces.wrapper import HTMWrapper
from htmproject.mappers.dir import DirMapper
from .protobuf_htm_wrapper_params import params as wrapper_params


class ProtoBufHTMWrapper(HTMWrapper):
    def __init__(self, *args, **kwargs):
        super(ProtoBufHTMWrapper, self).__init__(*args, **kwargs)

        self._config.add_default_params_section('wrapper', wrapper_params)

        self.__learning_protobuf = None
        self.__testing_protobuf = None

    def _get_learning_reader(self):
        return self.__get_reader(self.__learning_protobuf)

    def _get_testing_reader(self):
        return self.__get_reader(self.__testing_protobuf)

    def __get_reader(self, data_file):
        config = copy.deepcopy(self._config)
        config['iostreamer.data_file'] = data_file

        return self._get_reader_class()(config)

    def get_mapper(self, classes_list=None):
        if classes_list is None:
            classes_list = self._learning_reader.get_labels()
        # TODO: use the info directly form dataset.labels and group.label instead somehow
        return DirMapper(classes_list)

    def _get_reader_class(self):
        raise NotImplementedError()

    def _setup_learning(self):
        self.__learning_protobuf = self._config.get('wrapper.learning_protobuf_file')

        if self._class_name != 'default':
            self._learning_reader.set_class_filter(self._class_name)

        self._logger.debug('Learning protobuf path: %s' % (self.__learning_protobuf,))

        return super(ProtoBufHTMWrapper, self)._setup_learning()

    def _setup_testing(self, testing_mode=True):
        self.__testing_protobuf = self._config.get('wrapper.testing_protobuf_file')
        self._logger.debug('Testing protobuf path: %s' % (self.__testing_protobuf,))

        return super(ProtoBufHTMWrapper, self)._setup_testing(testing_mode)
