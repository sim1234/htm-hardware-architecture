import logging
import sys

from .wrapper_params import params as wrapper_params


class Wrapper(object):
    def __init__(self, config, class_name='default'):
        self._logger = logging.getLogger('htm')

        self._config = config
        self._class_name = class_name

        self._config.add_default_params_section('wrapper', wrapper_params)
        self._enable_visualizations = bool(self._config.get('wrapper.enable_visualizations'))
        self._partial_visualizations_frequency = int(self._config.get('wrapper.partial_visualizations_frequency'))
        self._enable_reporter = bool(self._config.get('wrapper.enable_reporter'))
        self._enable_progress = bool(self._config.get('wrapper.enable_progress'))

        self._logger.debug('Statistics visualizations enabled: %s (partial stats reported every %d iterations)' % (
            self._enable_visualizations,
            self._partial_visualizations_frequency
        ))
        self._logger.debug('HTML iterations reporter enabled: %s' % (self._enable_reporter,))
        self._logger.debug('Iterations progress display enabled: %s' % (self._enable_progress,))

        self._iteration_char = None
        self._reset_iteration_char()

        self._learn_set_up = False

    def update_config(self, config):
        # those settings cannot be changed
        core = self._config.get('core')
        encoder = self._config.get('encoder')

        self._config.update(config)
        self._config['core'] = core
        self._config['encoder'] = encoder

    def learn(self):
        if not self._learn_set_up:
            self._setup_learning()
            self._learn_set_up = True

        self._setup_testing(False)

        return self._start_learning()

    def test(self, testing_mode=True):
        self._setup_testing(testing_mode)

        return self._start_testing()

    def _log_iteration(self, i):
        if self._enable_progress and 0 < self._logger.getEffectiveLevel() < logging.WARNING:
            if i and i % 100 == 0:
                sys.stdout.write('\n')
            sys.stdout.write(self._iteration_char)
            sys.stdout.flush()

    def _log_iterations_done(self, elapsed_iterations):
        if self._enable_progress and 0 < self._logger.getEffectiveLevel() < logging.WARNING:
            sys.stdout.write('\n')
            sys.stdout.flush()

        self._logger.info('Elapsed iterations: %i' % (elapsed_iterations,))

    def _reset_iteration_char(self):
        self._iteration_char = '.'

    def _switch_iteration_char(self):
        self._iteration_char = 'x' if self._iteration_char == '.' else '.'

    def _setup_learning(self):
        raise NotImplementedError()

    def _setup_testing(self, testing_mode=True):
        raise NotImplementedError()

    def _start_learning(self):
        raise NotImplementedError()

    def _start_testing(self):
        raise NotImplementedError()
