params = {
    "enable_visualizations": True,
    "partial_visualizations_frequency": 1000,
    "enable_reporter": False,
    "enable_progress": True,

    "epochs": 1
}
