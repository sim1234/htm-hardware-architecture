from skimage.io import imread

from htmproject.interfaces.iostreamer.file import FileReader


class ImageFileReader(FileReader):
    """
    Reads frames from image files.
    """

    def _read_files(self):
        """

        :return:
        """
        while True:
            self._current_file = str(self._files_list.next())

            frame = imread(self._current_file)

            yield frame
