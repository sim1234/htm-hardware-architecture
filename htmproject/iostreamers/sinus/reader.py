import math

from htmproject.interfaces.iostreamer.reader import Reader


def rescale(X, A, B, C, D, force_float=False):
    retval = ((float(X - A) / (B - A)) * (D - C)) + C
    if not force_float and all(map(lambda x: type(x) == int, [X, A, B, C, D])):
        return int(round(retval))
    return retval

SAMPLES = 256
SCALE = 0, 255  # Output scale range

class SinusReader(Reader):
    def __init__(self, size, max_val):
        self.__size = size
        self.__max_val = max_val
        self.__current = None
        self.__data = self.__data_generator()

        angles = [rescale(i, 0, SAMPLES, 0, 360) for i in range(SAMPLES)]
        self.sin_table = [int(round(rescale(s, -1, 1, SCALE[0], SCALE[1]))) for s in [
            math.sin(math.radians(a)) for a in angles
        ]]

    def __data_generator(self):
        i = 0
        while i < self.__size:
            self.__current = self.sin_table[i % SAMPLES]
            yield self.__current
            i += 1

    def get_next_data_item(self):
        return self.__data.next()

    def get_data_set_size(self):
        return self.__size

    def open(self):
        pass

    def close(self):
        pass

    def get_current_data_item(self):
        return self.__current
