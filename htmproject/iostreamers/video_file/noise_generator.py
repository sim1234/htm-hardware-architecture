import numpy
import pyopencl as ocl
from pyopencl.clrandom import RanluxGenerator
from pyopencl.clmath import round as clround

from htmproject.utils.opencl import get_best_device


class NoiseGenerator(object):
    def __init__(self, cache_size=100):
        self.__queue = self.__get_queue()
        self.__gen = RanluxGenerator(self.__queue, luxury=3)

        self.__cache_normal = {}  # frames cache
        self.__cache_normal_gen = {}  # generators cache
        self.__index_normal = {}
        self.__min_values = None
        self.__max_values = None

        self.__cache_size = cache_size

    @staticmethod
    def __get_queue():
        device = get_best_device()
        context = ocl.Context([device])
        queue = ocl.CommandQueue(context)
        return queue

    def __reset_normal(self, sigma, frame_size):
        cn = self.__gen.normal(
            self.__queue,
            shape=(self.__cache_size * frame_size, ),
            dtype=numpy.float32,
            sigma=sigma
        )
        self.__cache_normal[sigma] = clround(cn).astype(numpy.int32)
        self.__index_normal[sigma] = 0

    def __get_normal(self, sigma, mu, frame_shape):
        frame_size = numpy.prod(frame_shape)
        self.__reset_normal(sigma, frame_size)
        while True:
            noise = self.__cache_normal[sigma][self.__index_normal[sigma]:self.__index_normal[sigma] + frame_size]\
                .reshape(frame_shape)
            self.__index_normal[sigma] += frame_size
            yield noise
            if self.__index_normal[sigma] >= self.__cache_size * frame_size:
                self.__reset_normal(sigma, frame_size)

    def add_normal_noise(self, frame, sigma, mu=0):
        return self.__add_normal_noise_ocl(frame, sigma, mu)

    def __add_normal_noise_ocl(self, frame, sigma, mu=0):
        if sigma not in self.__cache_normal_gen:
            self.__cache_normal_gen[sigma] = self.__get_normal(sigma, mu, frame.shape)
            self.__max_values = ocl.array.empty(self.__queue, shape=frame.shape, dtype=numpy.int32)
            self.__max_values.fill(255)
            self.__min_values = ocl.array.zeros(self.__queue, shape=frame.shape, dtype=numpy.int32)
        noise = self.__cache_normal_gen[sigma].next()
        clframe = ocl.array.to_device(self.__queue, frame)
        ret_frame = ocl.array.maximum(
            ocl.array.minimum(noise + clframe, self.__max_values),
            self.__min_values
        ).astype(numpy.uint8).map_to_host(flags=ocl.map_flags.READ)
        return ret_frame

    def __add_normal_noise_numpy(self, frame, sigma, mu=0):
        noise = numpy.round(numpy.random.normal(0, sigma, frame.shape))
        ret_frame = numpy.maximum(numpy.minimum(noise + frame, 255), 0).astype(numpy.uint8)
        return ret_frame
