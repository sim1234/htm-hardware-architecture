import cv2

from htmproject.interfaces.iostreamer.file import FileReader

try:
    # OpenCV 3
    CAP_PROP_FRAME_COUNT = cv2.CAP_PROP_FRAME_COUNT
except AttributeError:
    CAP_PROP_FRAME_COUNT = cv2.cv.CV_CAP_PROP_FRAME_COUNT


class VideoFileReader(FileReader):
    """
    Reads frames from *.{avi,mpg} video files.
    """

    def __init__(self, config, shuffling=True):
        super(VideoFileReader, self).__init__(config, ('avi', 'mpg'), shuffling)
        self._capture = None
        self._CAP_PROP_FRAME_COUNT = CAP_PROP_FRAME_COUNT

    def _read_files(self):
        raise NotImplementedError()

    def get_data_set_size(self, files_filter=lambda n: True):
        size = 0
        files_list = self.get_files_list()

        for f in files_list:
            if files_filter(f):
                capture = cv2.VideoCapture(f)
                size += capture.get(self._CAP_PROP_FRAME_COUNT)
                capture.release()

        return int(size)

    def close(self):
        """
        Close the video file.
        """
        if self._capture is not None:
            self._capture.release()

    def get_capture_prop(self, prop):
        return self._capture.get(prop)
