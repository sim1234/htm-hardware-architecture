import argparse
import copy
import logging
import sys
import os
from io import BytesIO

import cv2
import re

import skimage.io

from htmproject.configs.root import RootConfig
from htmproject.iostreamers.video_file import VideoFileChunkReader
from htmproject.interfaces.iostreamer.protobuf import ProtoBufWriter

logging.basicConfig(
    format='%(asctime)s : %(levelname)s : %(message)s',
    level=logging.INFO,
    stream=sys.stdout
)

try:
    # OpenCV 3
    CV_CAP_PROP_FRAME_WIDTH = cv2.CAP_PROP_FRAME_WIDTH
    CV_CAP_PROP_FRAME_HEIGHT = cv2.CAP_PROP_FRAME_HEIGHT
except AttributeError:
    CV_CAP_PROP_FRAME_WIDTH = cv2.cv.CV_CAP_PROP_FRAME_WIDTH
    CV_CAP_PROP_FRAME_HEIGHT = cv2.cv.CV_CAP_PROP_FRAME_HEIGHT


class VideoProtoBufDatasetGenerator(object):
    def __init__(self, input_dir, out_file, dataset_name=None):
        self.__abs_input_dir = os.path.abspath(input_dir)

        self._logger = logging.getLogger('htm')

        if dataset_name is None:
            dataset_name = os.path.basename(input_dir)
        self.__dataset_name = dataset_name

        config = RootConfig()
        config.set("iostreamer.in_dir", self.__abs_input_dir)
        config.set("iostreamer.data_file", "tmp.protobuf")
        config.set("iostreamer.extras_proto_path", "htmproject.iostreamers.video_protobuf.video_pb2")

        self.__file_reader = VideoFileChunkReader(config, shuffling=True)
        self.__tmp_writer = ProtoBufWriter(config)
        config_copy = copy.deepcopy(config)
        config_copy.set("iostreamer.data_file", out_file)
        self.__out_writer = ProtoBufWriter(config_copy)

    def run(self):
        self.__file_reader.open()
        self.__tmp_writer.open()

        root_re = self.__abs_input_dir + os.path.sep + '([\w_-]+)' + os.path.sep
        chunk = self.__file_reader.next_chunk()

        video_info_msg = self.__tmp_writer.extras.VideoInfo()
        video_info_msg.frame_width = int(self.__file_reader.get_capture_prop(CV_CAP_PROP_FRAME_WIDTH))
        video_info_msg.frame_height = int(self.__file_reader.get_capture_prop(CV_CAP_PROP_FRAME_HEIGHT))

        label_keys = [d for d in os.listdir(self.__abs_input_dir)
                      if os.path.isdir(os.path.join(self.__abs_input_dir, d))]
        labels = dict(zip(label_keys, range(0, len(label_keys))))

        groups_counter = 0
        items_counter = 0
        while chunk is not None:
            groups_counter += 1

            basename = os.path.basename(chunk['name'])
            label_re = re.compile(root_re + basename)
            label = label_re.match(chunk['name']).group(1)

            self._logger.info('Processing %s...' % (basename,))

            group_msg = self.__tmp_writer.proto.Group()
            group_msg.name = chunk['name']
            group_msg.label = labels[label]
            group_msg.size = chunk['size']

            items_counter += group_msg.size

            for i in range(group_msg.size):
                frame = self.__file_reader.get_next_data_item()
                buf = BytesIO()
                skimage.io.imsave(buf, frame, plugin='pil', format_str='PNG')
                item_msg = self.__tmp_writer.proto.Item()
                item_msg.value[:] = buf.getvalue()
                group_msg.items.extend([item_msg])

            self.__tmp_writer.save_message(group_msg)

            chunk = self.__file_reader.next_chunk()

        self.__file_reader.close()
        self.__tmp_writer.close()

        dataset_msg = self.__tmp_writer.proto.Dataset()
        dataset_msg.name = self.__dataset_name
        dataset_msg.groups = groups_counter
        dataset_msg.items = items_counter
        for key, num in labels.items():
            dataset_msg.labels[num] = key
        dataset_msg.extras.Pack(video_info_msg)

        self.__out_writer.open()
        self.__out_writer.save_message(dataset_msg)

        with open("tmp.protobuf", "rb") as tmp:
            self.__out_writer.file.write(tmp.read())

        self.__out_writer.close()

        os.unlink("tmp.protobuf")


def parse_args(args):
    parser = argparse.ArgumentParser(description="Helper script to convert hierarchical directories into protobuf")

    parser.add_argument(
        'input_dir'
    )

    parser.add_argument(
        'out_protobuf_path'
    )

    return parser.parse_args(args)


if __name__ == "__main__":
    args = parse_args(sys.argv[1:])

    generator = VideoProtoBufDatasetGenerator(args.input_dir, args.out_protobuf_path)
    generator.run()
