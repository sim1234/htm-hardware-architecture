import os

from htmproject.mappers.filenames import FileNamesMapper


class DirMapper(FileNamesMapper):
    @staticmethod
    def extract_name(v):
        p = os.path.dirname(v).split(os.path.sep)
        return p[-1]
