import os

from htmproject.interfaces.mapper import Mapper


class FileNamesMapper(Mapper):
    def __init__(self, names=None, list_to_extract=None):
        if names is not None:
            self._names = names
        elif list_to_extract is not None:
            self._names = self.__names_from_list(list_to_extract)
        else:
            self._names = []

    @staticmethod
    def extract_name(v):
        v = os.path.basename(v)
        return v[:v.index('-')]

    def __names_from_list(self, list_to_extract):
        names = [
            self.extract_name(v) for v in list_to_extract
        ]
        return list(set(names))
