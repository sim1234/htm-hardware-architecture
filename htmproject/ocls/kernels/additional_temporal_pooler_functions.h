#ifndef additional_temporal_pooler_functions_h
#define additional_temporal_pooler_functions_h
#include "mwc64x.cl"

/** \enum CellState_enum
 *  \brief Enum containing all available cell states.
 */
typedef enum {
  ACTIVE_CELL = 0x01,
  PREDICTIVE_CELL = 0x02,
  LEARN_CELL = 0x04
} CellState_enum;

/** \enum WhenState_enum
 *  \brief Enum containing all available time states.
 */
typedef enum {
  NOW,
  WAS
} WhenState_enum;

/** \struct Data
 *  \brief Struct containing pointers to all OpenCL buffers.
 */
typedef struct {
/** \brief Settings group
 *  \details m_settings[0] == m_cellsPerColumn,
 *  m_settings[1] == m_segmentsPerCell,
 *  m_settings[2] == m_synapsesPerSegment,
 *  m_settings[3] == m_isLearningActive,
 *  m_settings[4] == m_segmentActivationThreshold
 */
uint m_columnsCount;
uint m_cellsPerColumn;
uint m_segmentsPerCell;
uint m_synapsesPerSegment;
uint m_isLearningActive;
uint m_segmentActivationThreshold;
uint m_minSegmentActivationThreshold;
uint m_synapseConnectedPermanence; // divide by 100000
uint m_maxNewSynapses;
uint m_permanenceIncrement; // divide by 100000
uint m_permanenceDecrement; // divide by 100000
uint m_initialPermanence; // divide by 100000

/** Cell */
global uchar *m_cellState; // m_cellState & 0xF0 - WAS, m_cellState & 0x0F - NOW
global uchar *m_hasCellQueuedChanges;
global uint *m_newSegmentQueuedCount;
global uint *m_segmentsCount;
volatile global uint *m_isLearningCellInColumn;
local uint *m_usedLearningCells; // [0] cell, [1] column

/** Segment */
global uchar *m_isSegmentSequential;
global uchar *m_hasSegmentQueuedChanges;
global uchar *m_sequenceSegmentQueued;
//global uchar *m_segmentState;
global uint *m_synapsesCount;
global uint *m_newSynapsesCount;

/** Synapse */
global uint *m_synapsePermanence;
global uchar *m_synapsePermanenceQueued;
global uint *m_synapseTargetColumn;
global uint *m_synapseTargetCell;
//global uchar *m_synapseTargetCellState;

global mwc64x_state_t *m_randomNumberState;
} Data;

typedef struct {
  uint m_cellIdx;
  uint m_segmentIdx;
  bool m_anyCellFound;
  bool m_anySegmentFound;
} CellSearch;

typedef struct {
  uint m_segmentIdx;
  uint m_activeSynapses;
  bool m_anySegmentFound;
} SegmentSearch;

typedef struct {
  uint m_segmentIdx;
  bool m_segmentCreated;
} NewSegment;

typedef struct {
  uint m_segmentIdx;
  bool m_valid;
} ActiveSegment;

uchar getCellState(Data *data, uint columnIdx, uint cellIdx, WhenState_enum when);
void setCellState(Data *data, uint columnIdx, uint cellIdx, CellState_enum state);
Data makeDataStructure(constant uint *m_settings, global uchar *m_cellState, \
                       global uchar *m_hasCellQueuedChanges, global uint *m_newSegmentQueuedCount, global uint *m_segmentsCount, \
		               global uchar *m_isSegmentSequential, global uchar *m_hasSegmentQueuedChanges, \
		               global uchar *m_sequenceSegmentQueued, global uint *m_synapsesCount, global uint *m_newSynapsesCount,\
		               global uint *m_synapsePermanence, global uchar *m_synapsePermanenceQueued, \
		               global uint *m_synapseTargetColumn, global uint *m_synapseTargetCell, volatile global uint *m_isLearningCellInColumn,
		               local uint *m_usedLearningCells, global mwc64x_state_t *m_randomNumberState);
bool isSegmentSequential(Data *data, uint m_columnIdx, uint m_cellIdx, uint m_segment);
ActiveSegment getActiveSegment(Data *data, uint columnIdx, uint cellIdx, WhenState_enum when, CellState_enum cellState);
bool segmentActive(Data *data, uint columnIdx, uint cellIdx, uint segmentIdx, WhenState_enum when, CellState_enum cellState);
uint segmentActivity(Data *data, uint columnIdx, uint cellIdx, uint segmentIdx, WhenState_enum when, CellState_enum cellState);
CellSearch getBestMatchingCell(Data *data, uint columnIdx, WhenState_enum when);
SegmentSearch getBestMatchingSegment(Data *data, uint columnIdx, uint cellIdx, WhenState_enum when);
NewSegment queueNewSegment(Data *data, uint columnIdx, uint cellIdx, uchar isSegmentSequential);
void getLayerLearningCells(Data *data, unsigned int columnIdx);
void getSegmentActiveSynapses(Data *data, uint columnIdx, uint cellIdx, uint segmentIdx, WhenState_enum when, bool makeSegmentSequential, bool allowNewSynapses);
void adaptSegments(Data *data, uint columnIdx, uint cellIdx, bool positiveReinforcement);
void adaptCell(Data *data, uint columnIdx, uint cellIdx);

#endif // additional_temporal_pooler_functions_h