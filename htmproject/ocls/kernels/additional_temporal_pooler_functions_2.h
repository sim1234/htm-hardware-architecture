#ifndef additional_temporal_pooler_functions_2_h
#define additional_temporal_pooler_functions_2_h
#include "mwc64x.cl"

// defines are passed with build system
// cl.Program(self.ctx, fstr).build("-I " + kernels_dir + " " + self.build_defines)
// #define MAX_COLUMNS_PER_LAYER
// #define MAX_CELLS_PER_COLUMN
// #define MAX_SEGMENTS_PER_CELL
// #define MAX_SYNAPSES_PER_SEGMENT
// #define SEGMENT_ACTIVATION_THRESHOLD
// #define SEGMENT_MIN_ACTIVATION_THRESHOLD
// #define SYNAPSE_CONNECTED_PERMANENCE
// #define MAX_NEW_SYNAPSES
// #define SYNAPSE_PERMANENCE_INCREMENT // divide by 100000
// #define SYNAPSE_PERMANENCE_DECREMENT // divide by 100000
// #define SYNAPSE_INITIAL_PERMANENCE // divide by 100000

/** \enum CellState
 *  \brief Enum containing all available cell states.
 */
typedef enum {
  ACTIVE_CELL = 0x01,
  PREDICTIVE_CELL = 0x02,
  LEARN_CELL = 0x04
} CellState;

/** \enum WhenState
 *  \brief Enum containing all available time states.
 */
typedef enum {
  NOW, // 0x00
  WAS // 0x01
} WhenState;

/** \struct Data
 *  \brief Struct containing pointers to all OpenCL buffers.
 */
typedef struct {
/** Cell */
global uchar *m_cellState; // m_cellState & 0xF0 - WAS, m_cellState & 0x0F - NOW
global uchar *m_hasCellQueuedChanges;
global uint *m_newSegmentsQueuedCount;
global uint *m_segmentsCount;
global uchar *m_isLearningCellInColumn; // [get_global_size(0)] - NOW, [get_global_size(0) + 1] WAS
local uint *m_usedLearningCells; // [i + 0] - cell, [i + 1] - column

/** Segment */
global uchar *m_isSegmentSequential;
global uchar *m_hasSegmentQueuedChanges;
global uchar *m_sequenceSegmentQueued;
global uint *m_synapsesCount;
global uint *m_newSynapsesQueuedCount;

/** Synapse */
global uint *m_synapsePermanence;
global int *m_synapsePermanenceQueued;
global uint *m_synapseTargetColumn;
global uint *m_synapseTargetCell;

global mwc64x_state_t *m_randomNumberState;
} Data;

typedef struct {
  uint m_cellIdx;
  uint m_segmentIdx;
  bool m_anyCellFound;
  bool m_anySegmentFound;
} CellSearch;

typedef struct {
  uint m_segmentIdx;
  uint m_activeSynapses;
  bool m_anySegmentFound;
} SegmentSearch;

typedef struct {
  uint m_segmentIdx;
  bool m_segmentCreated;
} NewSegment;

typedef struct {
  uint m_segmentIdx;
  bool m_valid;
} ActiveSegment;

uchar getCellState(Data *data, uint columnIdx, uint cellIdx, WhenState when);
void setCellState(Data *data, uint columnIdx, uint cellIdx, CellState state);
Data makeDataStructure(global uchar *m_cellState, \
                       global uchar *m_hasCellQueuedChanges, global uint *m_newSegmentQueuedCount, global uint *m_segmentsCount, \
		               global uchar *m_isSegmentSequential, global uchar *m_hasSegmentQueuedChanges, \
		               global uchar *m_sequenceSegmentQueued, global uint *m_synapsesCount, global uint *m_newSynapsesQueuedCount,\
		               global uint *m_synapsePermanence, global int *m_synapsePermanenceQueued, \
		               global uint *m_synapseTargetColumn, global uint *m_synapseTargetCell, global uchar *m_isLearningCellInColumn,
		               local uint *m_usedLearningCells, global mwc64x_state_t *m_randomNumberState);
bool isSegmentSequential(Data *data, uint m_columnIdx, uint m_cellIdx, uint m_segment);
ActiveSegment getActiveSegment(Data *data, uint columnIdx, uint cellIdx, WhenState when, CellState cellState);
bool segmentActive(Data *data, uint columnIdx, uint cellIdx, uint segmentIdx, WhenState when, CellState cellState);
uint segmentActivity(Data *data, uint columnIdx, uint cellIdx, uint segmentIdx, WhenState when, CellState cellState);
CellSearch getBestMatchingCell(Data *data, uint columnIdx, WhenState when);
SegmentSearch getBestMatchingSegment(Data *data, uint columnIdx, uint cellIdx, WhenState when);
NewSegment queueNewSegment(Data *data, uint columnIdx, uint cellIdx, uchar isSegmentSequential);
void getLayerLearningCells(Data *data, unsigned int columnIdx);
void getSegmentActiveSynapses(Data *data, uint columnIdx, uint cellIdx, uint segmentIdx, WhenState when, bool makeSegmentSequential, bool allowNewSynapses);
void applySegmentsQueuedChanges(Data *data, uint columnIdx, uint cellIdx, bool positiveReinforcement);
void applyCellsQueuedChanges(Data *data, uint columnIdx, uint cellIdx);

#endif // additional_temporal_pooler_functions_2_h