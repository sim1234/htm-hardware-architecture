#include "mwc64x.cl"

kernel void test_random_generator(uint samplesPerStream, uint baseOffset, global mwc64x_state_t *randomNumberState, global uint *randomNumberOutput) {
    MWC64X_SeedStreams(randomNumberState, baseOffset, samplesPerStream);

    if(get_global_id(0) == 0) {
        for(uint i = 0; i < samplesPerStream; ++i) {
            *(randomNumberOutput + i) = MWC64X_NextUint(randomNumberState);
        }
    }
}