// kernel = single synapse
// TODO: quantize boost_data (preferably define-dependent)
__kernel void sp_overlap(
    const CONSTANT_OR_GLOBAL_IN_DATA bool *in_data, // input data - synapses*columns
    __global unsigned int *overlap_data, // overlap data
    __local unsigned int *local_data // local memory to share between work-items in the work-group
) {
    // TODO: types can be optimized depending on NUMBER_OF_SYNAPSES
    unsigned int i; // loop iterator
    const unsigned int lid = get_local_id(0);  // local synapse id in this column part
    const unsigned int column_part_id = get_group_id(0);
    const unsigned int column_id = column_part_id / OVERLAP_WORK_GROUPS_PER_COLUMN;
    const unsigned int column_part = column_part_id % OVERLAP_WORK_GROUPS_PER_COLUMN;
    const unsigned int local_size = get_local_size(0);
    const unsigned int synapse_id = column_id * NUMBER_OF_SYNAPSES + column_part * local_size + lid; // synapse id in input data

    if (synapse_id < (column_id + 1) * NUMBER_OF_SYNAPSES) {
        local_data[lid] = in_data[synapse_id];
    } else {
        local_data[lid] = 0;
    }
    barrier(CLK_LOCAL_MEM_FENCE);  // wait till all work-items have read their data

    for (i=1; i < local_size; i*=2) {
        if ((lid % (2*i)) == 0)
        {
            local_data[lid] = local_data[lid] + local_data[lid + i];
        }
        barrier(CLK_LOCAL_MEM_FENCE);  // wait till all work-items have saved their data
    }

    if (lid == 0) {
        overlap_data[column_part_id] = local_data[0];
    }
}

// kernel = single column
__kernel void sp_overlap_check(
    const CONSTANT_OR_GLOBAL_BOOST_DATA float *boost_data, // boost coefficients
    __global unsigned int *overlap_data // overlap data
) {
    const unsigned int column_id = get_global_id(0);
    unsigned int overlap = 0;
    unsigned int i; // loop iterator

    for (i = 0; i < OVERLAP_WORK_GROUPS_PER_COLUMN; i++) {
        overlap += overlap_data[column_id * OVERLAP_WORK_GROUPS_PER_COLUMN + i];
    }

    overlap_data[column_id * OVERLAP_WORK_GROUPS_PER_COLUMN] = (overlap > MIN_OVERLAP) ? ceil(overlap * boost_data[column_id]) : 0;
}

// kernel = single column
// TODO: calculate all const before passing them to kernel? data transfer vs execution time
// TODO: or move them to __local memory?
__kernel void sp_inhibition(
    const __global unsigned int *overlap_data,
    __global bool *out_data,
    const int inhibition_radius,
    const unsigned int inhibition_range, // 2*inhibition_radius + 1
    __local unsigned int *local_data  // local memory to share between work-items in the work-group
) {

    unsigned int i; // loop iterator
    int j; // loop iterator
    unsigned int current; // current column boosted overlap
    unsigned int index;
    unsigned int counter = 0; // counts how many times (during comparison) current value is bigger than a data stored in the local memory
    const unsigned int lid = get_local_id(0);
    const unsigned int gid = get_global_id(0);
    const unsigned int local_size = get_local_size(0);

    // copy all data from global to local memory
    for (i = lid, j = -inhibition_radius + gid; i < NUMBER_OF_COLUMNS + inhibition_range; i += local_size, j += local_size) {
        index = (j + NUMBER_OF_COLUMNS) % NUMBER_OF_COLUMNS;
        local_data[i] = overlap_data[index * OVERLAP_WORK_GROUPS_PER_COLUMN];
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    current = local_data[lid + inhibition_radius];

    // check inhibition only if column is active
    if (current > 0) {
        for (i = 0; i < inhibition_range; i++) {
            if (current > local_data[lid + i]) {
                counter++;
            }
        }

        // transfer to global memory
        i = max(((int) inhibition_range - (int) WINNERS_SET_SIZE), 0);
        // all active go in (i==0) or current column overlap is bigger than i others
        out_data[gid] = (i == 0 || counter > i) ? 1 : 0;
    } else {
        out_data[gid] = 0;
    }
}
