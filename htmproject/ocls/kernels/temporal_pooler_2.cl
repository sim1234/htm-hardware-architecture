#include "additional_temporal_pooler_functions_2.h"

kernel void initializeLayer(uchar isLearningActive, global uchar *cellState,
    global uchar *hasCellQueuedChanges, global uint *newSegmentsQueuedCount, global uint *segmentsCount,
    global uchar *isSegmentSequential, global uchar *hasSegmentQueuedChanges,
    global uchar *sequenceSegmentQueued, global uint *synapsesCount, global uint *newSynapsesQueuedCount,
    global uint *synapsePermanence, global int *synapsePermanenceQueued,
    global uint *synapseTargetColumn, global uint *synapseTargetCell, global uchar *isLearningCellInColumn,
    global mwc64x_state_t *randomNumberState, global uint *initRandomValues) {
    size_t columnIdx = get_global_id(0);

/* DEBUG PURPOSE
    if(get_global_id(0) == 0) {
        printf("MAX_COLUMNS_PER_LAYER %d,\n\
        MAX_CELLS_PER_COLUMN %d,\n\
        MAX_SEGMENTS_PER_CELL %d,\n\
        MAX_SYNAPSES_PER_SEGMENT %d,\n\
        SEGMENT_ACTIVATION_THRESHOLD %d,\n\
        SEGMENT_MIN_ACTIVATION_THRESHOLD %d,\n\
        SYNAPSE_CONNECTED_PERMANENCE %d,\n\
        MAX_NEW_SYNAPSES %d,\n\
        SYNAPSE_PERMANENCE_INCREMENT %d,\n\
        SYNAPSE_PERMANENCE_DECREMENT %d,\n\
        SYNAPSE_INITIAL_PERMANENCE %d\n\
        ",\
        MAX_COLUMNS_PER_LAYER,\
        MAX_CELLS_PER_COLUMN,\
        MAX_SEGMENTS_PER_CELL,\
        MAX_SYNAPSES_PER_SEGMENT,\
        SEGMENT_ACTIVATION_THRESHOLD,\
        SEGMENT_MIN_ACTIVATION_THRESHOLD,\
        SYNAPSE_CONNECTED_PERMANENCE,\
        MAX_NEW_SYNAPSES,\
        SYNAPSE_PERMANENCE_INCREMENT,\
        SYNAPSE_PERMANENCE_DECREMENT,\
        SYNAPSE_INITIAL_PERMANENCE\
        );
    }
    for(uint i = 0; i < 5000000; ++i) { }
    */

    // initialize random number generator
    MWC64X_SeedStreams(&randomNumberState[columnIdx], *(initRandomValues + columnIdx), 4294967296);

    // clear all global buffer data
    uint cellStart = columnIdx * MAX_CELLS_PER_COLUMN;
    for(uint cell = 0; cell < MAX_CELLS_PER_COLUMN; ++cell) {
        *(cellState + cellStart + cell) = 0;
        *(hasCellQueuedChanges + cellStart + cell) = 0;
        *(newSegmentsQueuedCount + cellStart + cell) = 0;
        *(segmentsCount + cellStart + cell) = 0;

        uint segmentStart = cellStart * MAX_SEGMENTS_PER_CELL + cell * MAX_SEGMENTS_PER_CELL;
        for(uint segment = 0; segment < MAX_SEGMENTS_PER_CELL; ++segment) {
            *(isSegmentSequential + segmentStart + segment) = 0;
            *(hasSegmentQueuedChanges + segmentStart + segment) = 0;
            *(sequenceSegmentQueued + segmentStart + segment) = 0;
            *(synapsesCount + segmentStart + segment) = 0;
            *(newSynapsesQueuedCount + segmentStart + segment) = 0;

            uint synapseStart = segmentStart * MAX_SYNAPSES_PER_SEGMENT + segment * MAX_SYNAPSES_PER_SEGMENT;
            for(uint synapse = 0; synapse < MAX_SYNAPSES_PER_SEGMENT; ++synapse) {
                *(synapsePermanence + synapseStart + synapse) = 0;
                *(synapsePermanenceQueued + synapseStart + synapse) = 0;
                *(synapseTargetColumn + synapseStart + synapse) = 0;
                *(synapseTargetCell + synapseStart + synapse) = 0;
            }
        }
    }

    if(columnIdx == 0) {
        isLearningCellInColumn[get_global_size(0)] = 0;
        isLearningCellInColumn[get_global_size(0) + 1] = 0;
    }

    *(isLearningCellInColumn + columnIdx)  = 0;
}

// time update, shift states, present states becomes past states
kernel void phaseZero(global uchar *cellState, global uchar *isLearningCellInColumn) {
    size_t columnIdx = get_global_id(0);

    uint cellStart = columnIdx * MAX_CELLS_PER_COLUMN;
    for(uint cell = 0; cell < MAX_CELLS_PER_COLUMN; ++cell) {
        *(cellState + cellStart + cell) = (*(cellState + cellStart + cell)) << 4;
    }

    if(columnIdx == 0) {
        isLearningCellInColumn[get_global_size(0) + 1] = isLearningCellInColumn[get_global_size(0)];
        isLearningCellInColumn[get_global_size(0)] = 0;
    }
    *(isLearningCellInColumn + columnIdx) = ((*(isLearningCellInColumn + columnIdx)) << 4) & 0xF0;
}

kernel void phaseOne(uchar isLearningActive, global uchar *cellState, \
                          global uchar *hasCellQueuedChanges, global uint *newSegmentsQueuedCount, global uint *segmentsCount,
                          global uchar *m_isSegmentSequential, global uchar *hasSegmentQueuedChanges, \
                          global uchar *sequenceSegmentQueued, global uint *synapsesCount, global uint *newSynapsesQueuedCount,\
                          global uint *synapsePermanence, global int *synapsePermanenceQueued, \
                          global uint *synapseTargetColumn, global uint *synapseTargetCell, global uchar *isLearningCellInColumn,
                          local uint *usedLearningCells, global mwc64x_state_t *randomNumberState, constant uchar *columnState) {
    size_t columnIdx = get_global_id(0);

    bool buPredicted = false;
    bool lcChosen = false;

    Data data = makeDataStructure(cellState,
        hasCellQueuedChanges, newSegmentsQueuedCount, segmentsCount,
        m_isSegmentSequential, hasSegmentQueuedChanges,
        sequenceSegmentQueued, synapsesCount, newSynapsesQueuedCount,
        synapsePermanence, synapsePermanenceQueued,
        synapseTargetColumn, synapseTargetCell, isLearningCellInColumn,
        usedLearningCells, randomNumberState);

    if(columnState[columnIdx]) {
        uint segmentIdx;
        ActiveSegment segment;
        for(uint cell = 0; cell < MAX_CELLS_PER_COLUMN; ++cell) {
            if(getCellState(&data, columnIdx, cell, WAS) & PREDICTIVE_CELL) {
                segment = getActiveSegment(&data, columnIdx, cell, WAS, ACTIVE_CELL);
                if(segment.m_valid == true) {
                    if(isSegmentSequential(&data, columnIdx, cell, segment.m_segmentIdx)) {
                        buPredicted = true;
                        setCellState(&data, columnIdx, cell, ACTIVE_CELL);
                        if(isLearningActive && segmentActive(&data, columnIdx, cell, segment.m_segmentIdx, WAS, LEARN_CELL)) {
                            lcChosen = true;
                            setCellState(&data, columnIdx, cell, LEARN_CELL);
                        }
                    }
                }
            }
        }

        if(!buPredicted) {
            for(uint cell = 0; cell < MAX_CELLS_PER_COLUMN; ++cell) {
                setCellState(&data, columnIdx, cell, ACTIVE_CELL);
            }
        }

        if(isLearningActive && !lcChosen) {
            CellSearch bestMatchingCell = getBestMatchingCell(&data, columnIdx, WAS);
            setCellState(&data, columnIdx, bestMatchingCell.m_cellIdx, LEARN_CELL);
            if(bestMatchingCell.m_anySegmentFound == true) {
                getSegmentActiveSynapses(&data, columnIdx, bestMatchingCell.m_cellIdx, bestMatchingCell.m_segmentIdx, WAS, true, true);
            } else {
                NewSegment newSegment = queueNewSegment(&data, columnIdx, bestMatchingCell.m_cellIdx, true);
                if(newSegment.m_segmentCreated == true) {
                    getSegmentActiveSynapses(&data, columnIdx, bestMatchingCell.m_cellIdx, newSegment.m_segmentIdx, WAS, true, true);

                    uint segmentStart = columnIdx * MAX_CELLS_PER_COLUMN * MAX_SEGMENTS_PER_CELL + bestMatchingCell.m_cellIdx * MAX_SEGMENTS_PER_CELL;
                    // remove segment, if no synapses are connected
                    if(newSynapsesQueuedCount[segmentStart + newSegment.m_segmentIdx] == 0) {
                        *(newSegmentsQueuedCount + columnIdx * MAX_CELLS_PER_COLUMN + bestMatchingCell.m_cellIdx) -= 1;
                    }
                }
            }
        }
    }
}

kernel void phaseTwo(uchar isLearningActive, global uchar *cellState, \
                          global uchar *hasCellQueuedChanges, global uint *newSegmentsQueuedCount, global uint *segmentsCount,
                          global uchar *isSegmentSequential, global uchar *hasSegmentQueuedChanges, \
                          global uchar *sequenceSegmentQueued, global uint *synapsesCount, global uint *newSynapsesQueuedCount,\
                          global uint *synapsePermanence, global int *synapsePermanenceQueued, \
                          global uint *synapseTargetColumn, global uint *synapseTargetCell, global uchar *isLearningCellInColumn,
                          local uint *usedLearningCells, global mwc64x_state_t *randomNumberState) {
    size_t columnIdx = get_global_id(0);

    Data data = makeDataStructure(cellState,
        hasCellQueuedChanges, newSegmentsQueuedCount, segmentsCount,
        isSegmentSequential, hasSegmentQueuedChanges,
        sequenceSegmentQueued, synapsesCount, newSynapsesQueuedCount,
        synapsePermanence, synapsePermanenceQueued,
        synapseTargetColumn, synapseTargetCell, isLearningCellInColumn,
        usedLearningCells, randomNumberState);

    // collect data about present learning cells in layer
    getLayerLearningCells(&data, columnIdx);

    for(uint cell = 0; cell < MAX_CELLS_PER_COLUMN; ++cell) {
        for(uint segment = 0; segment < MAX_SEGMENTS_PER_CELL; ++segment) {
            if(segmentActive(&data, columnIdx, cell, segment, NOW, ACTIVE_CELL)) {
                setCellState(&data, columnIdx, cell, PREDICTIVE_CELL);

                if(isLearningActive) {
                    getSegmentActiveSynapses(&data, columnIdx, cell, segment, NOW, false, false);

                    SegmentSearch bestMatchingSegment = getBestMatchingSegment(&data, columnIdx, cell, WAS);
                    if(bestMatchingSegment.m_anySegmentFound == true) {
                        getSegmentActiveSynapses(&data, columnIdx, cell, bestMatchingSegment.m_segmentIdx, WAS, false, true);
                    } else {
                        NewSegment newSegment = queueNewSegment(&data, columnIdx, cell, false);
                        if(newSegment.m_segmentCreated == true) {
                            getSegmentActiveSynapses(&data, columnIdx, cell, newSegment.m_segmentIdx, WAS, false, true);

                            uint segmentStart = columnIdx * MAX_CELLS_PER_COLUMN * MAX_SEGMENTS_PER_CELL + cell * MAX_SEGMENTS_PER_CELL;
                            // remove segment, if no synapses are connected
                            if(newSynapsesQueuedCount[segmentStart + newSegment.m_segmentIdx] == 0) {
                                *(newSegmentsQueuedCount + columnIdx * MAX_CELLS_PER_COLUMN + cell) -= 1;
                            }
                        }
                    }
                }
            }
        }
    }
}

kernel void phaseThree(uchar isLearningActive, global uchar *cellState, \
                          global uchar *hasCellQueuedChanges, global uint *newSegmentsQueuedCount, global uint *segmentsCount,
                          global uchar *isSegmentSequential, global uchar *hasSegmentQueuedChanges, \
                          global uchar *sequenceSegmentQueued, global uint *synapsesCount, global uint *newSynapsesQueuedCount,\
                          global uint *synapsePermanence, global int *synapsePermanenceQueued, \
                          global uchar *outputData) {
    size_t columnIdx = get_global_id(0);
    uchar outputStateColumn = 0;

    Data data = makeDataStructure(cellState,
        hasCellQueuedChanges, newSegmentsQueuedCount, segmentsCount,
        isSegmentSequential, hasSegmentQueuedChanges,
        sequenceSegmentQueued, synapsesCount, newSynapsesQueuedCount,
        synapsePermanence, synapsePermanenceQueued,
        NULL, NULL, NULL,
        NULL, NULL);

    if(isLearningActive) {
        for(uint cell = 0; cell < MAX_CELLS_PER_COLUMN; ++cell) {
            if(getCellState(&data, columnIdx, cell, NOW) & LEARN_CELL) {
                applyCellsQueuedChanges(&data, columnIdx, cell);
                applySegmentsQueuedChanges(&data, columnIdx, cell, true);
            } else if(((getCellState(&data, columnIdx, cell, NOW) & PREDICTIVE_CELL) == 0) && (getCellState(&data, columnIdx, cell, WAS) & PREDICTIVE_CELL)) {
                applyCellsQueuedChanges(&data, columnIdx, cell);
                applySegmentsQueuedChanges(&data, columnIdx, cell, false);
            }
            // removes updates if cell predicts too long
            else if((getCellState(&data, columnIdx, cell, WAS) & PREDICTIVE_CELL) && (getCellState(&data, columnIdx, cell, NOW) & PREDICTIVE_CELL)){
                *(newSegmentsQueuedCount + columnIdx * MAX_CELLS_PER_COLUMN + cell) = 0;
                *(hasCellQueuedChanges + columnIdx * MAX_CELLS_PER_COLUMN + cell) = false;
                uint segmentStart = columnIdx * MAX_CELLS_PER_COLUMN * MAX_SEGMENTS_PER_CELL + cell * MAX_SEGMENTS_PER_CELL;
                for(uint segment = 0; segment < MAX_SEGMENTS_PER_CELL; ++segment) {
                    hasSegmentQueuedChanges[segmentStart + segment] = 0;
                    newSynapsesQueuedCount[segmentStart + segment] = 0;
                }
            }
        }
    }

    // calculate TP output
    for(uint cell = 0; cell < MAX_CELLS_PER_COLUMN; ++cell) {
        if(getCellState(&data, columnIdx, cell, NOW) & (ACTIVE_CELL | PREDICTIVE_CELL)) {
            outputStateColumn = 1;
            break;
        }
    }
    *(outputData + columnIdx) = outputStateColumn;
}
