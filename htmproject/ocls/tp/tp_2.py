from time import time

import numpy as np
import pyopencl as cl
from htmproject.interfaces.ocl import OCL
import os

os.environ['PYOPENCL_COMPILER_OUTPUT'] = '1'

PRINT_TIME_RESULTS = 0

class TemporalPoolerOCL2(OCL):
    # TODO: Add ability to handle multiple layers of HTM!

    def __init__(self, build_defines, settings, work_group_size):
        super(TemporalPoolerOCL2, self).__init__([
            'temporal_pooler_2', 'additional_temporal_pooler_functions_2'
        ], build_defines)

        self.MAX_COLUMNS_PER_LAYER = settings['MAX_COLUMNS_PER_LAYER']
        self.MAX_CELLS_PER_COLUMN = settings['MAX_CELLS_PER_COLUMN']
        self.MAX_SEGMENTS_PER_CELL = settings['MAX_SEGMENTS_PER_CELL']
        self.MAX_SYNAPSES_PER_SEGMENT = settings['MAX_SYNAPSES_PER_SEGMENT']
        self.SEGMENT_ACTIVATION_THRESHOLD = settings['SEGMENT_ACTIVATION_THRESHOLD']
        self.SEGMENT_MIN_ACTIVATION_THRESHOLD = settings['SEGMENT_MIN_ACTIVATION_THRESHOLD']
        self.SYNAPSE_CONNECTED_PERMANENCE = settings['SYNAPSE_CONNECTED_PERMANENCE']
        self.MAX_NEW_SYNAPSES = settings['MAX_NEW_SYNAPSES']
        self.SYNAPSE_PERMANENCE_INCREMENT = settings['SYNAPSE_PERMANENCE_INCREMENT']
        self.SYNAPSE_PERMANENCE_DECREMENT = settings['SYNAPSE_PERMANENCE_DECREMENT']
        self.SYNAPSE_INITIAL_PERMANENCE = settings['SYNAPSE_INITIAL_PERMANENCE']

        self._work_group_size = work_group_size

        self._settings = None

        self._columns_per_layer = None

        self._cellStateBuffer = None
        self._hasCellQueuedChangesBuffer = None
        self._newSegmentsQueuedCountBuffer = None
        self._segmentsCountBuffer = None
        self._isSegmentSequentialBuffer = None
        self._hasSegmentQueuedChangesBuffer = None
        self._sequenceSegmentQueuedBuffer = None
        self._synapsesCountBuffer = None
        self._newSynapsesQueuedCountBuffer = None
        self._synapsePermanenceBuffer = None
        self._synapsePermanenceQueuedBuffer = None
        self._synapseTargetColumnBuffer = None
        self._synapseTargetCellBuffer = None
        self._isLearningCellInColumnBuffer = None
        self._usedLearningCellsBuffer = None

        self.outputData = None
        self.cellStateData = None
        self.hasCellQueuedChangesData = None
        self.newSegmentsQueuedCountData = None
        self.segmentsCountData = None
        self.isSegmentSequentialData = None
        self.hasSegmentQueuedChangesData = None
        self.sequenceSegmentQueuedData = None
        self.synapsesCountData = None
        self.newSynapsesQueuedCountData = None
        self.synapsePermanenceData = None
        self.synapsePermanenceQueuedData = None
        self.synapseTargetColumnData = None
        self.synapseTargetCellData = None
        self.isLearningCellInColumnData = None

        self._outputBuffer = None
        self._valuesFromSpBuffer = None

        self._mwc64x_state_Buffer = None

        self.randomValueData = None
        self._randomValueBuffer = None

        self._isLearningActive = True

        self.total_elapsed_time = 0

    def __toggle_learning(self, state=True):
        self.__settings[4] = state
        self.__settingsBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR,
                                          hostbuf=self.__settings)

    def prepare_kernel(self, columns, settings, work_group_size):
        self.__work_group_size = work_group_size
        self.__prepare_kernel(columns, settings)

    def __prepare_kernel(self, columns, settings):
        self._columns_per_layer = columns

        self._load_kernel()

        self.outputData = np.zeros(self._columns_per_layer).astype(np.uint8)
        self._outputBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE, self.outputData.nbytes)

        self.cellStateData = np.zeros(self._columns_per_layer * self.MAX_CELLS_PER_COLUMN).astype(np.uint8)
        self._cellStateBuffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY,
                                          np.dtype(
                                              np.uint8).itemsize * self._columns_per_layer * self.MAX_CELLS_PER_COLUMN)

        self.hasCellQueuedChangesData = np.zeros(self._columns_per_layer *
                                                 self.MAX_CELLS_PER_COLUMN).astype(np.uint8)
        self._hasCellQueuedChangesBuffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY,
                                                     np.dtype(np.uint8).itemsize * self._columns_per_layer *
                                                     self.MAX_CELLS_PER_COLUMN)

        self.newSegmentsQueuedCountData = np.zeros(self._columns_per_layer *
                                                   self.MAX_CELLS_PER_COLUMN).astype(np.uint32)
        self._newSegmentsQueuedCountBuffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY,
                                                       np.dtype(np.uint32).itemsize * self._columns_per_layer *
                                                       self.MAX_CELLS_PER_COLUMN)

        self.segmentsCountData = np.zeros(self._columns_per_layer *
                                          self.MAX_CELLS_PER_COLUMN).astype(np.uint32)
        self._segmentsCountBuffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY,
                                              np.dtype(np.uint32).itemsize * self._columns_per_layer *
                                              self.MAX_CELLS_PER_COLUMN)

        self.isSegmentSequentialData = np.zeros(self._columns_per_layer *
                                                self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL).astype(np.uint8)
        self._isSegmentSequentialBuffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY,
                                                    np.dtype(np.uint8).itemsize * self._columns_per_layer *
                                                    self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL)

        self.hasSegmentQueuedChangesData = np.zeros(self._columns_per_layer *
                                                    self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL).astype(
            np.uint8)
        self._hasSegmentQueuedChangesBuffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY,
                                                        np.dtype(np.uint8).itemsize * self._columns_per_layer *
                                                        self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL)

        self.sequenceSegmentQueuedData = np.zeros(self._columns_per_layer *
                                                  self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL).astype(
            np.uint8)
        self._sequenceSegmentQueuedBuffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY,
                                                      np.dtype(np.uint8).itemsize * self._columns_per_layer *
                                                      self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL)

        self.synapsesCountData = np.zeros(self._columns_per_layer *
                                          self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL).astype(np.uint32)
        self._synapsesCountBuffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY,
                                              np.dtype(np.uint32).itemsize * self._columns_per_layer *
                                              self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL)

        self.newSynapsesQueuedCountData = np.zeros(self._columns_per_layer *
                                                   self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL).astype(
            np.uint32)
        self._newSynapsesQueuedCountBuffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY,
                                                       2 * np.dtype(np.uint32).itemsize * self._columns_per_layer *
                                                       self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL)

        self.synapsePermanenceData = np.zeros(self._columns_per_layer *
                                              self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL * self.MAX_SYNAPSES_PER_SEGMENT).astype(
            np.uint32)
        self._synapsePermanenceBuffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY,
                                                  np.dtype(np.uint32).itemsize * self._columns_per_layer *
                                                  self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL * self.MAX_SYNAPSES_PER_SEGMENT)

        self.synapsePermanenceQueuedData = np.zeros(self._columns_per_layer *
                                                    self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL * self.MAX_SYNAPSES_PER_SEGMENT).astype(
            np.int32)
        self._synapsePermanenceQueuedBuffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY,
                                                        np.dtype(np.int32).itemsize * self._columns_per_layer *
                                                        self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL * self.MAX_SYNAPSES_PER_SEGMENT)

        self.synapseTargetColumnData = np.zeros(self._columns_per_layer *
                                                self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL * self.MAX_SYNAPSES_PER_SEGMENT).astype(
            np.uint32)
        self._synapseTargetColumnBuffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY,
                                                    np.dtype(np.uint32).itemsize * self._columns_per_layer *
                                                    self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL * self.MAX_SYNAPSES_PER_SEGMENT)

        self.synapseTargetCellData = np.zeros(self._columns_per_layer *
                                              self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL * self.MAX_SYNAPSES_PER_SEGMENT).astype(
            np.uint32)
        self._synapseTargetCellBuffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY,
                                                  np.dtype(np.uint32).itemsize * self._columns_per_layer *
                                                  self.MAX_CELLS_PER_COLUMN * self.MAX_SEGMENTS_PER_CELL * self.MAX_SYNAPSES_PER_SEGMENT)

        self.isLearningCellInColumnData = np.zeros(self._columns_per_layer + 2).astype(np.uint8)
        self._isLearningCellInColumnBuffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY,
                                                       np.dtype(np.uint8).itemsize * (self._columns_per_layer + 2))

        self._usedLearningCellsBuffer = cl.LocalMemory(
            2 * self._work_group_size * self.MAX_NEW_SYNAPSES * np.dtype(np.uint32).itemsize)

        self._mwc64x_state_Buffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY,
                                              2 * self._columns_per_layer * np.dtype(np.uint32).itemsize)

        self.randomValueData = np.random.randint(0, 10 ** 6, self._columns_per_layer, np.uint32)
        self._randomValueBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR,
                                            hostbuf=self.randomValueData)

        exec_evt = self.program.initializeLayer(
            self.queue, (self._columns_per_layer,), (self._work_group_size,),
            np.uint8(self._isLearningActive), self._cellStateBuffer,
            self._hasCellQueuedChangesBuffer, self._newSegmentsQueuedCountBuffer,
            self._segmentsCountBuffer,
            self._isSegmentSequentialBuffer, self._hasSegmentQueuedChangesBuffer,
            self._sequenceSegmentQueuedBuffer, self._synapsesCountBuffer,
            self._newSynapsesQueuedCountBuffer,
            self._synapsePermanenceBuffer, self._synapsePermanenceQueuedBuffer,
            self._synapseTargetColumnBuffer, self._synapseTargetCellBuffer,
            self._isLearningCellInColumnBuffer, self._mwc64x_state_Buffer, self._randomValueBuffer)
        exec_evt.wait()
        elapsed = 1e-9 * (exec_evt.profile.end - exec_evt.profile.start)

        print("Execution time of initializeLayer: %g s" % elapsed)

    def execute_kernel(self, output_values_from_sp, is_learning_enabled=True):
        # TODO: add is_learning_enabled as a param
        # TODO: add layer_index as a param
        elapsed = 0

        self._isLearningActive = is_learning_enabled
        output_values_from_sp = np.array(output_values_from_sp).astype(np.uint8)

        self._valuesFromSpBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR,
                                              hostbuf=output_values_from_sp)

        exec_evt = self.program.phaseZero(
            self.queue, (self._columns_per_layer,), (self._work_group_size,),
            self._cellStateBuffer, self._isLearningCellInColumnBuffer)
        exec_evt.wait()
        elapsed += 1e-9 * (exec_evt.profile.end - exec_evt.profile.start)
        elapsed_kernel = 1e-9 * (exec_evt.profile.end - exec_evt.profile.start)
        if PRINT_TIME_RESULTS:
            print("phaseZero: %g s" % elapsed_kernel)
        exec_evt = self.program.phaseOne(
            self.queue, (self._columns_per_layer,), (self._work_group_size,),
            np.uint8(self._isLearningActive), self._cellStateBuffer,
            self._hasCellQueuedChangesBuffer, self._newSegmentsQueuedCountBuffer,
            self._segmentsCountBuffer,
            self._isSegmentSequentialBuffer, self._hasSegmentQueuedChangesBuffer,
            self._sequenceSegmentQueuedBuffer, self._synapsesCountBuffer,
            self._newSynapsesQueuedCountBuffer,
            self._synapsePermanenceBuffer, self._synapsePermanenceQueuedBuffer,
            self._synapseTargetColumnBuffer, self._synapseTargetCellBuffer,
            self._isLearningCellInColumnBuffer, self._usedLearningCellsBuffer,
            self._mwc64x_state_Buffer, self._valuesFromSpBuffer)
        exec_evt.wait()
        elapsed += 1e-9 * (exec_evt.profile.end - exec_evt.profile.start)
        elapsed_kernel = 1e-9 * (exec_evt.profile.end - exec_evt.profile.start)
        if PRINT_TIME_RESULTS:
            print("phaseOne: %g s" % elapsed_kernel)
        exec_evt = self.program.phaseTwo(
            self.queue, (self._columns_per_layer,), (self._work_group_size,),
            np.uint8(self._isLearningActive), self._cellStateBuffer,
            self._hasCellQueuedChangesBuffer, self._newSegmentsQueuedCountBuffer,
            self._segmentsCountBuffer,
            self._isSegmentSequentialBuffer, self._hasSegmentQueuedChangesBuffer,
            self._sequenceSegmentQueuedBuffer, self._synapsesCountBuffer,
            self._newSynapsesQueuedCountBuffer,
            self._synapsePermanenceBuffer, self._synapsePermanenceQueuedBuffer,
            self._synapseTargetColumnBuffer, self._synapseTargetCellBuffer,
            self._isLearningCellInColumnBuffer, self._usedLearningCellsBuffer,
            self._mwc64x_state_Buffer)
        exec_evt.wait()
        elapsed += 1e-9 * (exec_evt.profile.end - exec_evt.profile.start)
        elapsed_kernel = 1e-9 * (exec_evt.profile.end - exec_evt.profile.start)
        if PRINT_TIME_RESULTS:
            print("phaseTwo: %g s" % elapsed_kernel)
        exec_evt = self.program.phaseThree(
            self.queue, (self._columns_per_layer,), (self._work_group_size,),
            np.uint8(self._isLearningActive), self._cellStateBuffer,
            self._hasCellQueuedChangesBuffer, self._newSegmentsQueuedCountBuffer,
            self._segmentsCountBuffer,
            self._isSegmentSequentialBuffer, self._hasSegmentQueuedChangesBuffer,
            self._sequenceSegmentQueuedBuffer, self._synapsesCountBuffer,
            self._newSynapsesQueuedCountBuffer,
            self._synapsePermanenceBuffer, self._synapsePermanenceQueuedBuffer,
            self._outputBuffer)
        exec_evt.wait()
        elapsed += 1e-9 * (exec_evt.profile.end - exec_evt.profile.start)
        elapsed_kernel = 1e-9 * (exec_evt.profile.end - exec_evt.profile.start)
        if PRINT_TIME_RESULTS:
            print("phaseThree: %g s" % elapsed_kernel)
        read_evt = cl.enqueue_read_buffer(self.queue, self._outputBuffer, self.outputData)
        read_evt.wait()
        elapsed_read = 1e-9 * (read_evt.profile.end - read_evt.profile.start)
        self.total_elapsed_time += elapsed + elapsed_read
        if PRINT_TIME_RESULTS:
            print("Execution time of all GPU kernels: %g s" % elapsed)
            print("Read time (OpenCL events): %g s" % elapsed_read)
            print("Read time + execution: %g s" % (elapsed_read + elapsed))

        return self.outputData

    def get_total_elapsed_time(self):
        return self.total_elapsed_time

