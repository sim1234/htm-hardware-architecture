import os
import shutil
from operator import itemgetter

from htmproject.interfaces.reporter import Reporter, HTMExtractor
from htmproject.utils.templates_mixin import TemplatesMixin

from .reporter_params import params as logger


class HTMLReporter(Reporter, TemplatesMixin):
    """
    Reporter generating collection of HTML files
    """

    def __init__(self, htm, config):
        """

        :param config: Configuration object
        :return:
        """
        Reporter.__init__(self, htm, config)
        TemplatesMixin.__init__(self)

        self.config.add_default_params_section('reporter', logger)

        self.__log_dir = self.config.get("reporter.log_dir")

        self.extractor = HTMExtractor()

        self._add_templates({
            'iteration': 'iteration.hbs.html',
            'index': 'index.hbs.html',
            'legend': 'legend.hbs.html',
            'config': 'configuration.hbs.html',
            'goto': 'navigation-goto.hbs.html'
        })

    def start(self):
        """
        Prepare for logging
        """
        self.__setup_log_dir()
        self.__log_config()
        self.__log_legend()

    def stop(self):
        """
        Finish logging
        """
        self.__log_index(self.iteration_index - 1)
        self.__log_goto(self.iteration_index - 1)

    def log_iteration(self, input_item, output_item):
        """
        Log single iteration
        :param input_item:
        :param output_item:
        :return:
        """
        input_item = input_item.tolist()
        output_item = output_item.tolist()

        data = self.extractor.extract(self.htm, input_item)
        data['iteration_index'] = self.iteration_index
        data['prev_iteration_index'] = self.iteration_index - 1 if self.iteration_index > 0 else False
        data['next_iteration_index'] = self.iteration_index + 1
        data['input_item'] = input_item
        data['output_item'] = output_item

        self._render_template(
            'iteration',
            data,
            os.path.join(self.__log_dir, 'iteration-%i.html' % (self.iteration_index, ))
        )

        self.iteration_index += 1

    def _get_script_dir(self):
        return TemplatesMixin._get_script_dir(self)

    def __setup_log_dir(self):
        if os.path.exists(self.__log_dir):
            shutil.rmtree(self.__log_dir)
        os.makedirs(self.__log_dir)
        shutil.copytree(os.path.join(self._tpl_dir, 'css'), os.path.join(self.__log_dir, 'css'))
        shutil.copytree(os.path.join(self._tpl_dir, 'js'), os.path.join(self.__log_dir, 'js'))

    def __log_config(self):
        core_config = [{
            'key': k,
            'value': v
        } for k, v in self.config.get('core').items()]
        wrapper_config = [{
            'key': k,
            'value': v
        } for k, v in self.config.get('cli').items()]

        core_config.sort(key=itemgetter('key'))
        wrapper_config.sort(key=itemgetter('key'))

        data = {
            'core_config': core_config,
            'wrapper_config': wrapper_config
        }

        self._render_template('config', data, os.path.join(self.__log_dir, 'configuration.html'))

    def __log_legend(self):
        data = {}
        self._render_template('legend', data, os.path.join(self.__log_dir, 'legend.html'))

    def __log_index(self, last_iteration_index):
        per_group = max(10, min(100, last_iteration_index / 100))

        data = {
            'groups': [
                {
                    'start_index': j,
                    'end_index': min(j + per_group - 1, last_iteration_index),
                    'iterations': [i for i in range(j, min(j + per_group, last_iteration_index + 1))]
                } for j in range(0, last_iteration_index + 1, per_group)
            ],
            'last_iteration_index': last_iteration_index
        }

        self._render_template('index', data, os.path.join(self.__log_dir, 'index.html'))

    def __log_goto(self, last_iteration_index):
        data = {
            'last_iteration_index': last_iteration_index
        }

        self._render_template('goto', data, os.path.join(self.__log_dir, 'navigation-goto.html'))

    def get_log_dir(self):
        return self.__log_dir
