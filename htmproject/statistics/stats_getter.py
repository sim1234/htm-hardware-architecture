import logging
import os
import shutil
import subprocess
import time
import sys
import multiprocessing
import math
import operator
import itertools

from htmproject.statistics.instance_generator import InstanceGenerator
from htmproject.statistics.stats_renderer import StatsRenderer


class StatsGetter(object):
    def __init__(self, config_path, statistics_config_path, save_pickles=False, use_pickles=False, max_processes=None,
                 instance_log='info', histograms=False, visualizations=True):
        self.__logger = logging.getLogger('htm')

        self.__instance_generator = InstanceGenerator(config_path, statistics_config_path, use_pickles)

        self.__active_processes = []
        self.__finished_processes = []
        self.__processes_queue = []
        self.__max_processes = int(math.floor(multiprocessing.cpu_count()*0.75)) if not max_processes else max_processes
        self.__save_pickles = save_pickles
        self.__use_pickles = use_pickles
        self.__instance_log = instance_log
        self.__dump_histograms = histograms

        self.__renderer = StatsRenderer(
            dir_path=self.__instance_generator.get_config_root(),
            config_path=config_path,
            statistics_config_path=statistics_config_path,
            visualizations=visualizations
        )

    def run(self):
        for instance_data in self.__instance_generator:
            self.__queue_instance(instance_data)

        while self.__process_queue():
            time.sleep(5)

        self.__renderer.render()

        for item in self.__finished_processes:
            self.__clean_instance_data(item)

    def __clean_instance_data(self, process_data):
        os.remove(process_data['config'])

        if not self.__use_pickles and process_data['learning_dir'] is not None:
            shutil.rmtree(process_data['learning_dir'])
        if process_data['testing_dir'] is not None:
            shutil.rmtree(process_data['testing_dir'])

    def __queue_instance(self, data):
        process_args = [
            'htmproject'
        ]

        if not self.__use_pickles:
            process_args.append('learn')

            if self.__save_pickles:
                process_args.append('--o-pickle=%s' % (data['htm_pickle_path'], ))
                process_args.append('--c-pickle=%s' % (data['classifier_pickle_path'], ))
        else:
            process_args.append('test')
            process_args.append('--i-pickle=%s' % (data['htm_pickle_path'],))
            process_args.append('--c-pickle=%s' % (data['classifier_pickle_path'],))

        process_args.append('--modular' if data['setup'] == 'multiple_htms' else '--no-modular')
        process_args.append('--log=%s' % (self.__instance_log, ))
        process_args.append('--c-stats=%s' % (data['stats_path'], ))

        if self.__dump_histograms:
            process_args.append('--histograms=%s' % (data['histograms_path'], ))

        process_args.append(data['config_path'])

        self.__processes_queue.append({
            'args': process_args,
            'config': data['config_path'],
            'learning_dir': data['learning_dir'],
            'testing_dir': data['testing_dir'],
            'stats': data['stats_path'],
            'process': None,
            'out': data['out_path']
        })

    def __process_queue(self):
        active_processes = []

        for item in self.__active_processes:
            if item['process'].poll() is not None:
                self.__logger.info('Instance %s done.' % (
                    os.path.basename(item['config']),
                ))
                self.__finished_processes.append(item)
            else:
                active_processes.append(item)
        self.__active_processes = active_processes

        to_run = itertools.ifilterfalse(operator.itemgetter('process'), self.__processes_queue)
        for i in xrange(len(self.__active_processes), self.__max_processes):
            try:
                item = to_run.next()
                out = open(item['out'], 'w')
                item['process'] = subprocess.Popen(item['args'], stdout=out, stderr=subprocess.STDOUT)
                self.__active_processes.append(item)
                self.__logger.info('Running %s...' % (
                    os.path.basename(item['config']),
                ))
            except StopIteration:
                break

        if not len(self.__active_processes):
            return False

        return True
