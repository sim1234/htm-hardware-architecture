import numpy as np


def aggregate_bits(data):
    """    
    Aggregates list of bools into numpy array of 32 bit int.
    data - list of bools
    result - numpy array of 32 bit int
    """
    
    bit_counter = 0  # how many bits were read from a list
    int_data = 0 # chunk of 32 bit long data to be written to the numpy array
    numpy_array_int = np.empty(0, dtype=np.uint32)
    
    # 0 bit padding (makes sure that the list is multiple of 32)
    if len(data) % 32:
        data = (lambda f : f + [0 for i in range(32 - (len(f) % 32))])(data)
    
    # reverse data to account for endianess
    data = reversed(data)
    
    for datum in data:
        int_data = int_data << 1 | datum
        if bit_counter == 31:
            numpy_array_int = np.hstack((numpy_array_int, int_data))
            bit_counter = 0
            int_data = 0
        else:
            bit_counter += 1
    
    # reverse an order of the array 
    numpy_array_int_changed_order = np.empty_like(numpy_array_int)
    numpy_array_int_changed_order[:] = numpy_array_int[::-1]
    numpy_array_int_changed_order = numpy_array_int_changed_order.astype(np.int32)
            
    return numpy_array_int_changed_order


def disaggregate_bits(aggregated_array):
    """
    Disaggregates list of dense 32-bit integer array into sparse 32-bit integer
    :param aggregated_array: array in a dense form.
    :return: array in a sparse form
    """
    list_of_ints = []
    for array_item in aggregated_array:
        for i in range(32): # it is assumed that ints are 32 bit long
            if array_item >> i:
                list_of_ints.append(1)

    return list_of_ints
