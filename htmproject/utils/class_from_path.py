import importlib


def get_class_from_path(path, desc):
    pkg_index = path.rfind('.')

    if pkg_index == -1:
        raise ValueError(msg='Invalid %s class path. Please specify the module path.' % (desc,))

    cls = path[pkg_index + 1:]
    pkg = path[:pkg_index]

    imported_module = importlib.import_module(pkg)
    return getattr(imported_module, cls)
