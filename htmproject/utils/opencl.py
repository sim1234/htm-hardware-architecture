import pyopencl as ocl


def get_best_device():
    """
    Finds all the available devices and uses the best among them.
    :return:
    """
    devices = []
    best = [
        ocl.device_type.ACCELERATOR,
        ocl.device_type.GPU,
        ocl.device_type.CPU,
        ocl.device_type.DEFAULT,
        ocl.device_type.ALL
    ]

    if ocl.get_cl_header_version()[1] > 1:
        best.insert(0, ocl.device_type.CUSTOM)

    for platform in ocl.get_platforms():
        for device in platform.get_devices():
            devices.append((device, device.type))

    devices.sort(cmp=lambda x, y: cmp(best.index(x[1]), best.index(y[1])))

    return devices[0][0]
