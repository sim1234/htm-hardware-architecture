import os
import datetime

from htmproject.interfaces.visualizer import Visualizer
from htmproject.utils.templates_mixin import TemplatesMixin
from .visualizer_params import params as visualizer_params


class MarkdownVisualizer(Visualizer, TemplatesMixin):
    def __init__(self, analyzer, config):
        Visualizer.__init__(self, analyzer, config)
        TemplatesMixin.__init__(self)

        self._config.add_default_params_section('visualizer.markdown', visualizer_params)
        self.__output_dir = self._config.get('visualizer.markdown.output_dir')

        if not os.path.exists(self.__output_dir):
            os.makedirs(self.__output_dir)

        self._add_templates({
            'config': 'config.hbs.md'
        })
        self.__save_config()

    def __save_config(self):
        time = str(datetime.datetime.now())

        self._render_template(
            'config',
            {'config': self._config},
            os.path.join(self.__output_dir, '%s_config.md' % (time, ))
        )

    def _get_script_dir(self):
        return TemplatesMixin._get_script_dir(self)

    def visualize_complete(self, prefix='', iterations_offset=0):
        pass
