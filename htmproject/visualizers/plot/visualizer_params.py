params = {
    "output_dir": "visualizer_plot_output",
    "global_plots": {
        "percent_of_active_inputs": True,
        "percent_of_active_outputs": True
    },
    "chunk_plots": {
        "histograms": False,
        "chunk_histograms": True,
        "class_histograms": True,
        "cumulative_class_histograms": True,
        "class_similarity": False,
        "inter_class_similarity": False
    },
    "per_layer_plots": {
        "percent_of_active_columns": False,
        "inhibition_radius": False,
        "overlap": False,
        "boost": False,
        "cell_active_[%_of_layer_capacity]": False,
        "cell_predictive_[%_of_layer_capacity]": False,
        "cell_learning_[%_of_layer_capacity]": False,
        "segment_active_[%_of_layer_capacity]": False,
        "segment_sequential_[%_of_layer_capacity]": False,
        "segment_learning_[%_of_layer_capacity]": False
    }
}
