from htmproject.wrappers.htm.images.adaptive import AdaptiveImagesHTMWrapper
from htmproject.encoders.gray_adaptive_frame import GrayAdaptiveFrameEncoder


class GrayAdaptiveImagesHTMWrapper(AdaptiveImagesHTMWrapper):
    def _setup_encoder(self):
        self._encoder = GrayAdaptiveFrameEncoder(self._config)
