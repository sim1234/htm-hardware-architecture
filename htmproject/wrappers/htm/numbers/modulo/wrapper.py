from htmproject.iostreamers.modulo.reader import ModuloReader
from htmproject.wrappers.htm.numbers.wrapper import NumbersHTMWrapper


class ModuloNumbersHTMWrapper(NumbersHTMWrapper):
    def _get_learning_reader(self):
        return ModuloReader(4000, self._encoder.spaceSize())

    def _get_testing_reader(self):
        return ModuloReader(self._encoder.spaceSize(), self._encoder.spaceSize())
