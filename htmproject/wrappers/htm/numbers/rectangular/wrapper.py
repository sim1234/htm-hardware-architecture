from htmproject.iostreamers.rectangular.reader import RectangularReader
from htmproject.wrappers.htm.numbers.wrapper import NumbersHTMWrapper


class RectangularNumbersHTMWrapper(NumbersHTMWrapper):
    def _get_learning_reader(self):
        return RectangularReader(4000, self._encoder.spaceSize(), 4)

    def _get_testing_reader(self):
        return RectangularReader(self._encoder.spaceSize(), self._encoder.spaceSize(), 4)
