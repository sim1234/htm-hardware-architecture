from htmproject.iostreamers.sinus.reader import SinusReader
from htmproject.wrappers.htm.numbers.wrapper import NumbersHTMWrapper


class SinusNumbersHTMWrapper(NumbersHTMWrapper):
    def _get_learning_reader(self):
        return SinusReader(4000, self._encoder.spaceSize())

    def _get_testing_reader(self):
        return SinusReader(self._encoder.spaceSize(), self._encoder.spaceSize())
