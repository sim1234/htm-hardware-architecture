from htmproject.encoders.scalar import ScalarEncoder
from htmproject.interfaces.wrapper import HTMWrapper
from htmproject.mappers.scalar.mapper import ScalarMapper


class NumbersHTMWrapper(HTMWrapper):
    def __init__(self, config, class_name='default', htm=None):
        super(NumbersHTMWrapper, self).__init__(config, class_name, htm)
        self.__counter = 0

    def _setup_encoder(self):
        self._encoder = ScalarEncoder(self._config)

    def get_mapper(self, classes_list=None):
        return ScalarMapper()

    def _get_name(self, is_learning=True):
        if is_learning:
            cur = self._learning_reader.get_current_data_item()
        else:
            cur = self._testing_reader.get_current_data_item()
        self.__counter += 1
        return '%i.%i' % (cur, self.__counter)
