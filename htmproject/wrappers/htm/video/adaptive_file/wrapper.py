from htmproject.encoders.adaptive_frame import AdaptiveFrameEncoder
from htmproject.interfaces.wrapper import FileHTMWrapper, ChunkMixin
from htmproject.iostreamers.video_file import VideoFileChunkReader


class AdaptiveVideoFileHTMWrapper(FileHTMWrapper, ChunkMixin):
    def _setup_encoder(self):
        self._encoder = AdaptiveFrameEncoder(self._config)

    def _get_reader_class(self):
        return VideoFileChunkReader
