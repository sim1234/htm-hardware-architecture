from htmproject.wrappers.htm.video.adaptive_file import AdaptiveVideoFileHTMWrapper


class AdaptiveVideoHistogramsFileHTMWrapper(AdaptiveVideoFileHTMWrapper):

    def _run_chunk(self, chunk, analyzer, offset=0, report=False):
        i = 0
        try:
            for i in xrange(chunk['size']):
                self._iterator.iterate()

                encoded_input = self._iterator.get_encoded_input()
                encoded_output = self._iterator.get_encoded_output()

                analyzer.save_iteration_stats(encoded_input, encoded_output)

                if report:
                    self._reporter.log_iteration(encoded_input, encoded_output)

                self._log_iteration(offset + i)
        except StopIteration:
            self._logger.debug('\nError while reading frame %i (out of %i) from %s chunk.' % (
                i,
                chunk['size'],
                chunk['name']
            ))
            raise StopIteration()

        self._switch_iteration_char()

        analyzer.save_chunk_info(chunk['name'], chunk['size'])
        analyzer.reset_chunk_outputs()
