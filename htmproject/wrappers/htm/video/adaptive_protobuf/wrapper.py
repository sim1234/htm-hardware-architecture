from htmproject.encoders.adaptive_frame import AdaptiveFrameEncoder
from htmproject.interfaces.wrapper import ProtoBufHTMWrapper, ChunkMixin
from htmproject.iostreamers.video_protobuf import VideoProtoBufReader


class AdaptiveVideoProtoBufHTMWrapper(ProtoBufHTMWrapper, ChunkMixin):
    def _setup_encoder(self):
        self._encoder = AdaptiveFrameEncoder(self._config)

    def _get_reader_class(self):
        return VideoProtoBufReader
