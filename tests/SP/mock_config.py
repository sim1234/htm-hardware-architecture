from htmproject.interfaces.config import Config

mock_config = Config({
    'core': {
        'htm': {
            'layers': 1,
            'columns_per_layer': [10],
            'cells_per_column': [0]
        },
        'sp': {
            'synapses_per_column': [4],
            'min_overlap': [1],
            'winners_set_size': [2]
        }
    }
})
