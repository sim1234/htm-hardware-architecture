from htmproject.configs.json import JSONConfig

class Settings():
    def __init__(self, config_name):
        config = JSONConfig(config_name)

        self.columns_amount = config.get('core.htm.columns_per_layer')
        self.max_new_synapses = config.get('core.tp.max_number_of_new_connections')
        self.cells_per_column = config.get('core.htm.cells_per_column')
        self.segments_per_cell = config.get('core.tp.max_number_of_segments')
        self.synapses_per_segment = config.get('core.tp.max_number_of_synapses_per_segment')
        self.is_learning_active = config.get('core.tp.is_learning_active')
        self.activation_threshold = config.get('core.tp.segment_activation_threshold')
        self.min_activation_threshold = config.get('core.tp.min_segment_activation_threshold')
        self.synapse_connected_permanence = config.get('core.tp.connected_perm')
        self.permanence_increment = config.get('core.tp.perm_inc')
        self.permanence_decrement = config.get('core.tp.perm_dec')
        self.initial_permanence = config.get('core.tp.init_perm')
        self.work_group_size = config.get('core.tp.work_group_size')

        self.settings = []
        self.settings.append(self.columns_amount[0])
        self.settings.append(self.cells_per_column[0])
        self.settings.append(self.segments_per_cell)
        self.settings.append(self.synapses_per_segment)
        self.settings.append(self.is_learning_active)
        self.settings.append(self.activation_threshold)
        self.settings.append(self.min_activation_threshold)
        self.settings.append(self.synapse_connected_permanence * 100000)
        self.settings.append(self.max_new_synapses)
        self.settings.append(self.permanence_increment * 100000)
        self.settings.append(self.permanence_decrement * 100000)
        self.settings.append(self.initial_permanence * 100000)

        self.settings_2 = {
            'MAX_COLUMNS_PER_LAYER': self.columns_amount[0],
            'MAX_CELLS_PER_COLUMN': self.cells_per_column[0],
            'MAX_SEGMENTS_PER_CELL': self.segments_per_cell,
            'MAX_SYNAPSES_PER_SEGMENT': self.synapses_per_segment,
            'SEGMENT_ACTIVATION_THRESHOLD': self.activation_threshold,
            'SEGMENT_MIN_ACTIVATION_THRESHOLD': self.min_activation_threshold,
            'SYNAPSE_CONNECTED_PERMANENCE': self.synapse_connected_permanence * 100000,
            'MAX_NEW_SYNAPSES': self.max_new_synapses,
            'SYNAPSE_PERMANENCE_INCREMENT': self.permanence_increment * 100000,
            'SYNAPSE_PERMANENCE_DECREMENT': self.permanence_decrement * 100000,
            'SYNAPSE_INITIAL_PERMANENCE': self.initial_permanence * 100000
        }

    def get_build_defines(self):
        build_define = '-D MAX_COLUMNS_PER_LAYER=' + str(self.settings_2.get('MAX_COLUMNS_PER_LAYER')) \
        + ' -D MAX_CELLS_PER_COLUMN=' + str(self.settings_2.get('MAX_CELLS_PER_COLUMN')) \
        + ' -D MAX_SEGMENTS_PER_CELL=' + str(self.settings_2.get('MAX_SEGMENTS_PER_CELL')) \
        + ' -D MAX_SYNAPSES_PER_SEGMENT=' + str(self.settings_2.get('MAX_SYNAPSES_PER_SEGMENT')) \
        + ' -D SEGMENT_ACTIVATION_THRESHOLD=' + str(self.settings_2.get('SEGMENT_ACTIVATION_THRESHOLD')) \
        + ' -D SEGMENT_MIN_ACTIVATION_THRESHOLD=' + str(self.settings_2.get('SEGMENT_MIN_ACTIVATION_THRESHOLD')) \
        + ' -D SYNAPSE_CONNECTED_PERMANENCE=' + str(int(self.settings_2.get('SYNAPSE_CONNECTED_PERMANENCE'))) \
        + ' -D MAX_NEW_SYNAPSES=' + str(self.settings_2.get('MAX_NEW_SYNAPSES')) \
        + ' -D SYNAPSE_PERMANENCE_INCREMENT=' + str(int(self.settings_2.get('SYNAPSE_PERMANENCE_INCREMENT'))) \
        + ' -D SYNAPSE_PERMANENCE_DECREMENT=' + str(int(self.settings_2.get('SYNAPSE_PERMANENCE_DECREMENT'))) \
        + ' -D SYNAPSE_INITIAL_PERMANENCE=' + str(int(self.settings_2.get('SYNAPSE_INITIAL_PERMANENCE')))

        return build_define

    def get_settings(self):
        return self.settings

    def get_settings_2(self):
        return self.settings_2