import unittest
from tests.UnitTests.tp_unit_test import OclTp
import numpy as np
from time import time
from settings import Settings

np.set_printoptions(threshold=np.nan)

class TestAll(unittest.TestCase):
    def setUp(self):
        print "Test case: ", self.__module__, self._testMethodName

        self.settings_manager = Settings('configs/basic.json')

    def tearDown(self):
        pass

    def test_basic(self):
        self.ocl_temporalpooler = OclTp('temporal_pooler', work_group_size=self.settings_manager.work_group_size)
        self.ocl_temporalpooler.prepare_buffers(self.settings_manager.columns_amount[0], self.settings_manager.get_settings())
        self.ocl_temporalpooler.prepare_kernel(0, 0)

        inD1 = [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        inD2 = [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1]
        inD3 = [0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0]

        for i in range(0, 800):
            if i % 3 == 0:
                inD = inD1
            if i % 3 == 1:
                inD = inD2
            if i % 3 == 2:
                inD = inD3

            start = time()

            self.ocl_temporalpooler.execute_kernel(inD)

            if i > 796:
                self.ocl_temporalpooler.toggle_learning(0)
                self.ocl_temporalpooler.execute_kernel(inD)
                self.ocl_temporalpooler.collect_data()
                print "input data: ", inD, "output data: ", self.ocl_temporalpooler.outputData
                # print "isLearningCellInColumnData", self.ocl_temporalpooler.isLearningCellInColumnData
                # print "cellStateData", self.ocl_temporalpooler.cellStateData
                # print "segmentsCountData", self.ocl_temporalpooler.segmentsCountData
                # print "synapsesCountData", (self.ocl_temporalpooler.synapsesCountData)
                # print "isSegmentSequentialData", (self.ocl_temporalpooler.isSegmentSequentialData)
                # print "synapseTargetCellData", (self.ocl_temporalpooler.synapseTargetCellData)
                # print "synapseTargetColumnData", (self.ocl_temporalpooler.synapseTargetColumnData)
                # print "synapsePermanenceData", (self.ocl_temporalpooler.synapsePermanenceData)


        print "ind1 + ind2 + ind3: ", np.add(np.add(inD1, inD2), inD3)
        print "outputData", self.ocl_temporalpooler.outputData

        self.assertTrue(np.array_equal(self.ocl_temporalpooler.outputData, np.add(np.add(inD1, inD2), inD3)))

        end = time()
        print("Execution time of kernels (Python time module): ", end - start, "s")
