import unittest
from tests.UnitTests.tp_2_unit_test import OclTp
import numpy as np
from time import time
from settings import Settings

np.set_printoptions(threshold=np.nan)

class TestAll(unittest.TestCase):
    def setUp(self):
        print "Test case: ", self.__module__, self._testMethodName

        self.settings_manager = Settings('configs/basic.json')

    def tearDown(self):
        pass

    def test_basic(self):
        self.ocl_temporalpooler = OclTp('temporal_pooler_2', self.settings_manager.get_settings_2(),
                                        build_defines=self.settings_manager.get_build_defines(),
                                        work_group_size=self.settings_manager.work_group_size)
        self.ocl_temporalpooler.prepare_buffers(self.settings_manager.get_settings_2()['MAX_COLUMNS_PER_LAYER'])
        self.ocl_temporalpooler.prepare_kernel(0)

        inD1 = np.zeros(self.settings_manager.get_settings_2()['MAX_COLUMNS_PER_LAYER'], dtype=np.int)
        inD1[0:10] = 1
        inD2 = np.zeros(self.settings_manager.get_settings_2()['MAX_COLUMNS_PER_LAYER'], dtype=np.int)
        inD2[10:20] = 1
        inD3 = np.zeros(self.settings_manager.get_settings_2()['MAX_COLUMNS_PER_LAYER'], dtype=np.int)
        inD3[20:30] = 1
        inD4 = np.zeros(self.settings_manager.get_settings_2()['MAX_COLUMNS_PER_LAYER'], dtype=np.int)
        inD4[30:40] = 1
        inD5 = np.zeros(self.settings_manager.get_settings_2()['MAX_COLUMNS_PER_LAYER'], dtype=np.int)
        inD5[40:50] = 1

        for i in range(0, 50):
            if i % 5 == 0:
                inD = inD1
            if i % 5 == 1:
                inD = inD2
            if i % 5 == 2:
                inD = inD3
            if i % 5 == 3:
                inD = inD4
            if i % 5 == 4:
                inD = inD5

            self.ocl_temporalpooler.execute_kernel(inD)

            if i > -1:
                pass
                # self.ocl_temporalpooler.toggle_learning(0)
                # self.ocl_temporalpooler.execute_kernel(inD)
                self.ocl_temporalpooler.collect_data()
                print "input data:  ", inD, "\noutput data: ", self.ocl_temporalpooler.outputData
                # print "newSynapsesQueuedCount", (self.ocl_temporalpooler.newSynapsesQueuedCountData)
                # print "newSegmentsQueuedCountData", (self.ocl_temporalpooler.newSegmentsQueuedCountData)
                # print "isLearningCellInColumnData", self.ocl_temporalpooler.isLearningCellInColumnData
                # print "cellStateData", self.ocl_temporalpooler.cellStateData
                # print "segmentsCountData", self.ocl_temporalpooler.segmentsCountData
                # print "synapsesCountData", (self.ocl_temporalpooler.synapsesCountData)
                # print "isSegmentSequentialData", (self.ocl_temporalpooler.isSegmentSequentialData)
                # print "synapseTargetCellData", (self.ocl_temporalpooler.synapseTargetCellData)
                # print "synapseTargetColumnData", (self.ocl_temporalpooler.synapseTargetColumnData)
                # print "synapsePermanenceData", (self.ocl_temporalpooler.synapsePermanenceData)