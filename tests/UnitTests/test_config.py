import unittest
from settings import Settings

class TestConfig(unittest.TestCase):
    def setUp(self):
        print "Test case: ", self.__module__, self._testMethodName

        self.settings_manager = Settings('configs/test_config.json')

    def tearDown(self):
        pass

    def test_basic_config(self):
        self.assertEqual(self.settings_manager.columns_amount[0], 10)
        self.assertEqual(self.settings_manager.cells_per_column[0], 3)
        self.assertEqual(self.settings_manager.max_new_synapses, 7)
        self.assertEqual(self.settings_manager.work_group_size, 2)
        self.assertEqual(self.settings_manager.segments_per_cell, 6)
        self.assertEqual(self.settings_manager.synapses_per_segment, 10)
        self.assertEqual(self.settings_manager.is_learning_active, 1)
        self.assertEqual(self.settings_manager.activation_threshold, 5)
        self.assertEqual(self.settings_manager.min_activation_threshold, 2)
        self.assertEqual(self.settings_manager.synapse_connected_permanence, 0.5)
        self.assertEqual(self.settings_manager.permanence_increment, 0.004)
        self.assertEqual(self.settings_manager.permanence_decrement, 0.003)
        self.assertEqual(self.settings_manager.initial_permanence, 0.45)