import unittest
from htmproject.configs.json import JSONConfig
from htmproject.interfaces.ocl import *
from htmproject.interfaces.ocl import OCL
import pyopencl as ocl
import os
import sys

class OclStub(OCL):
    def __init__(self, kernel_name):
        super(OclStub, self).__init__(kernel_name)

    def _load_kernel(self, show_kernel=False):
        f = open(self.kernel_path + ".cl")

        kernels_dir = self._get_kernels_dir()

        fstr = f.read()
        f.close()
        f = open(os.path.join(kernels_dir, "additional_temporal_pooler_functions.cl"))
        fstr += f.read()
        f.close()
        if show_kernel:
            print fstr

        self.program = ocl.Program(self.ctx, fstr). \
            build("-I " + kernels_dir)

class TestOcl(unittest.TestCase):
    def setUp(self):
        print "Test case: ", self.__module__, self._testMethodName

    def tearDown(self):
        pass

    def test_basic_build(self):
        self.ocl_temporalpooler = OclStub('temporal_pooler')
        self.ocl_temporalpooler._load_kernel()

    def test_devices_info(self):
        devices = []
        for platform in ocl.get_platforms():
            for device in platform.get_devices():
                devices.append((device, device.type))
        print devices
