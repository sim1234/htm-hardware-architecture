import unittest
from tp_unit_test import OclTp
from settings import Settings

class TestPhaseOne(unittest.TestCase):
    def setUp(self):
        print "Test case: ", self.__module__, self._testMethodName

        self.settings_manager = Settings('configs/basic.json')

    def tearDown(self):
        pass

    def test_basic(self):
        self.ocl_temporalpooler = OclTp('temporal_pooler', "-D PHASEZERO -D PHASETWO -D PHASETHREE -D PHASEFOUR", work_group_size=self.settings_manager.work_group_size)

        self.ocl_temporalpooler.prepare_buffers(self.settings_manager.columns_amount[0], self.settings_manager.get_settings())

        self.ocl_temporalpooler.prepare_kernel(0, 0)

        inD = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

        self.ocl_temporalpooler.execute_kernel(inD)
        self.ocl_temporalpooler.collect_data()

        print "input data: ", inD, "output data: ", self.ocl_temporalpooler.outputData