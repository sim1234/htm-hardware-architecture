import unittest
import numpy as np
from htmproject.interfaces.ocl import OCL
import pyopencl as cl
from time import time

class OCLStubRandomGenerator(OCL):
    def _load_kernel(self, show_kernel=False):
        kernels_dir = self._get_kernels_dir()

        f = open(self.kernel_path + ".cl")
        fstr = f.read()
        f.close()

        if show_kernel:
            print fstr

        self.program = cl.Program(self.ctx, fstr). \
            build("-I " + kernels_dir)

class TestRandomGenerator(unittest.TestCase):
    def setUp(self):
        print "Test case: ", self.__module__, self._testMethodName

    def tearDown(self):
        pass

    def test_random_numpy(self):
        start = time()
        rand_numpy = np.random.randint(2**32, size=1000000)
        end = time()
        print("np.random.randint time (Python time module): ", end - start, "s")

        print "Unique: ", np.unique(rand_numpy).size
        print "Diff between rand_numpy.size and unique: ", rand_numpy.size - np.unique(rand_numpy).size
        print "Average: ", np.average(rand_numpy) / 2 ** 32

    def test_opencl_random_generator(self):
        samples_per_Stream = 1000000

        ocl = OCLStubRandomGenerator("random_generator_test")

        random_number_data = np.zeros(samples_per_Stream).astype(np.uint32)

        random_number_buffer = cl.Buffer(ocl.ctx, cl.mem_flags.READ_WRITE,
                                                      random_number_data.nbytes)
        random_number_state_buffer = cl.Buffer(ocl.ctx, cl.mem_flags.READ_WRITE, 2 * np.dtype(np.uint32).itemsize)

        test_random_generator_knl = ocl.program.test_random_generator
        test_random_generator_knl.set_scalar_arg_dtypes([np.uint32, np.uint32, None, None])
        exec_evt = test_random_generator_knl(ocl.queue, [1, 1], None,
                                             samples_per_Stream, np.random.randint(0, 10 ** 6, 1, np.uint32),
                                             random_number_state_buffer, random_number_buffer)
        exec_evt.wait()
        elapsed = 1e-9 * (exec_evt.profile.end - exec_evt.profile.start)
        print("Execution time of random_generator_test: %g s" % elapsed)

        cl.enqueue_read_buffer(ocl.queue, random_number_buffer, random_number_data).wait()

        print "Unique: ", np.unique(random_number_data).size
        print "Diff between randomNumberData.size and unique: ", random_number_data.size - np.unique(random_number_data).size
        print "Average: ", np.average(random_number_data) / 2**32

