import unittest
import numpy as np
from htmproject.configs.json import JSONConfig
from htmproject.core.htm import *

np.set_printoptions(threshold=np.nan)


class TestTpBasic(unittest.TestCase):
    def setUp(self):
        print "Test case: ", self.__module__, self._testMethodName

    def tearDown(self):
        pass

    def test_basic(self):
        config = JSONConfig('configs/basic3.json')
        columns = 30

        htm = HTM(config, columns)

        inD1 = np.zeros(columns, dtype=np.int)
        inD1[0:15] = 1
        inD2 = np.zeros(columns, dtype=np.int)
        inD2[15:30] = 1

        layer = htm.get_layers()[0]

        for i in range(0, 10):
            if i % 2 == 0:
                inD = inD1
            else:
                inD = inD2

            layer.shift_states()

            for j in range(0, columns):
                if inD[j]:
                    layer.columns[j].set_active(True)
                else:
                    layer.columns[j].set_active(False)

            htm.get_tp().update_layer_state(layer)

            output = htm.get_tp().get_layer_output(layer)

            print i, ": input data: ", inD, "output data: ", output.astype(int)