import numpy as np
from htmproject.interfaces.ocl import *
import pyopencl as cl
import os

os.environ['PYOPENCL_COMPILER_OUTPUT'] = '1'

class OclTp(OCL):
    def __init__(self, kernel_name, build_defines="", work_group_size=0):
        assert(work_group_size > 0)
        self.build_defines = build_defines

        super(OclTp, self).__init__(kernel_name)

        self._work_group_size = work_group_size

        self._settings = None

        self._columns_per_layer = None

        self._settingsBuffer = None
        self._cellStateBuffer = None
        self._hasCellQueuedChangesBuffer = None
        self._newSegmentQueuedCountBuffer = None
        self._segmentsCountBuffer = None
        self._isSegmentSequentialBuffer = None
        self._hasSegmentQueuedChangesBuffer = None
        self._sequenceSegmentQueuedBuffer = None
        self._synapsesCountBuffer = None
        self._newSynapsesCountBuffer = None
        self._synapsePermanenceBuffer = None
        self._synapsePermanenceQueuedBuffer = None
        self._synapseTargetColumnBuffer = None
        self._synapseTargetCellBuffer = None
        self._isLearningCellInColumnBuffer = None
        self._usedLearningCellsBuffer = None

        self.outputData = None
        self.cellStateData = None
        self.hasCellQueuedChangesData = None
        self.newSegmentQueuedCountData = None
        self.segmentsCountData = None
        self.isSegmentSequentialData = None
        self.hasSegmentQueuedChangesData = None
        self.sequenceSegmentQueuedData = None
        self.synapsesCountData = None
        self.newSynapsesCountData = None
        self.synapsePermanenceData = None
        self.synapsePermanenceQueuedData = None
        self.synapseTargetColumnData = None
        self.synapseTargetCellData = None
        self.isLearningCellInColumnData = None

        self._outputBuffer = None
        self._valuesFromSpBuffer = None

        self._mwc64x_state_Buffer = None

        self.randomValueData = None
        self._randomValueBuffer = None

    def toggle_learning(self, state=True):
        self._settings[4] = state
        self._settingsBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR,
                                          hostbuf=self._settings)

    def prepare_buffers(self, columns, settings):
        self._prepare_buffers(columns, settings)

    def prepare_kernel(self, columns, settings):
        self._prepare_kernel(columns, settings)

    def _prepare_buffers(self, columns, settings):
        self._settings = settings

        self._columns_per_layer = columns
        self._settings = np.array(settings).astype(np.uint32)

        self._load_kernel()

        self.outputData = np.zeros(self._columns_per_layer).astype(np.uint8)
        self._outputBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE, self.outputData.nbytes)

        self._settingsBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR,
                                          hostbuf=self._settings)

        self.cellStateData = np.zeros(self._columns_per_layer * self._settings[1]).astype(np.uint8)
        self._cellStateBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE,
                                           np.dtype(np.uint8).itemsize * self._columns_per_layer * self._settings[1])

        self.hasCellQueuedChangesData = np.zeros(self._columns_per_layer *
                                                      self._settings[1]).astype(np.uint8)
        self._hasCellQueuedChangesBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE,
                                                      np.dtype(np.uint8).itemsize * self._columns_per_layer *
                                                      self._settings[1])

        self.newSegmentQueuedCountData = np.zeros(self._columns_per_layer *
                                                       self._settings[1]).astype(np.uint32)
        self._newSegmentQueuedCountBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE,
                                                       np.dtype(np.uint32).itemsize * self._columns_per_layer *
                                                       self._settings[1])

        self.segmentsCountData = np.zeros(self._columns_per_layer *
                                               self._settings[1]).astype(np.uint32)
        self._segmentsCountBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE,
                                               np.dtype(np.uint32).itemsize * self._columns_per_layer *
                                               self._settings[1])

        self.isSegmentSequentialData = np.zeros(self._columns_per_layer *
                                                     self._settings[1] * self._settings[2]).astype(np.uint8)
        self._isSegmentSequentialBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE,
                                                     np.dtype(np.uint8).itemsize * self._columns_per_layer *
                                                     self._settings[1] * self._settings[2])

        self.hasSegmentQueuedChangesData = np.zeros(self._columns_per_layer *
                                                         self._settings[1] * self._settings[2]).astype(np.uint8)
        self._hasSegmentQueuedChangesBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE,
                                                         np.dtype(np.uint8).itemsize * self._columns_per_layer *
                                                         self._settings[1] * self._settings[2])

        self.sequenceSegmentQueuedData = np.zeros(self._columns_per_layer *
                                                       self._settings[1] * self._settings[2]).astype(np.uint8)
        self._sequenceSegmentQueuedBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE,
                                                       np.dtype(np.uint8).itemsize * self._columns_per_layer *
                                                       self._settings[1] * self._settings[2])

        self.synapsesCountData = np.zeros(self._columns_per_layer *
                                               self._settings[1] * self._settings[2]).astype(np.uint32)
        self._synapsesCountBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE,
                                               np.dtype(np.uint32).itemsize * self._columns_per_layer *
                                               self._settings[1] * self._settings[2])

        self.newSynapsesCountData = np.zeros(self._columns_per_layer *
                                                  self._settings[1] * self._settings[2]).astype(np.uint32)
        self._newSynapsesCountBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE,
                                                  np.dtype(np.uint32).itemsize * self._columns_per_layer *
                                                  self._settings[1] * self._settings[2])

        self.synapsePermanenceData = np.zeros(self._columns_per_layer *
                                                   self._settings[1] * self._settings[2] * self._settings[3]).astype(np.uint32)
        self._synapsePermanenceBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE,
                                                   np.dtype(np.uint32).itemsize * self._columns_per_layer *
                                                   self._settings[1] * self._settings[2] * self._settings[3])

        self.synapsePermanenceQueuedData = np.zeros(self._columns_per_layer *
                                                         self._settings[1] * self._settings[2] * self._settings[3]).astype(np.uint8)
        self._synapsePermanenceQueuedBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE,
                                                         np.dtype(np.uint8).itemsize * self._columns_per_layer *
                                                         self._settings[1] * self._settings[2] * self._settings[3])

        self.synapseTargetColumnData = np.zeros(self._columns_per_layer *
                                                     self._settings[1] * self._settings[2] * self._settings[3]).astype(np.uint32)
        self._synapseTargetColumnBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE,
                                                     np.dtype(np.uint32).itemsize * self._columns_per_layer *
                                                     self._settings[1] * self._settings[2] * self._settings[3])

        self.synapseTargetCellData = np.zeros(self._columns_per_layer *
                                                   self._settings[1] * self._settings[2] * self._settings[3]).astype(np.uint32)
        self._synapseTargetCellBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE,
                                                   np.dtype(np.uint32).itemsize * self._columns_per_layer *
                                                   self._settings[1] * self._settings[2] * self._settings[3])

        self.isLearningCellInColumnData = np.zeros(self._columns_per_layer + 2).astype(np.uint32)
        self._isLearningCellInColumnBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE,
                                                   np.dtype(np.uint32).itemsize * (self._columns_per_layer + 2))

        self._usedLearningCellsBuffer = cl.LocalMemory(2 * self._work_group_size * settings[8] * np.dtype(np.uint32).itemsize)

        self._mwc64x_state_Buffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE, 2 * self._columns_per_layer * np.dtype(np.uint32).itemsize)

        self.randomValueData = np.random.randint(0, 10**6, self._columns_per_layer, np.uint32)
        self._randomValueBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR,
                                            hostbuf=self.randomValueData)

    def collect_data(self):
        cl.enqueue_read_buffer(self.queue, self._cellStateBuffer, self.cellStateData).wait()
        cl.enqueue_read_buffer(self.queue, self._hasCellQueuedChangesBuffer, self.hasCellQueuedChangesData).wait()
        cl.enqueue_read_buffer(self.queue, self._newSegmentQueuedCountBuffer, self.newSegmentQueuedCountData).wait()
        cl.enqueue_read_buffer(self.queue, self._segmentsCountBuffer, self.segmentsCountData).wait()
        cl.enqueue_read_buffer(self.queue, self._isSegmentSequentialBuffer, self.isSegmentSequentialData).wait()
        cl.enqueue_read_buffer(self.queue, self._hasSegmentQueuedChangesBuffer, self.hasSegmentQueuedChangesData).wait()
        cl.enqueue_read_buffer(self.queue, self._sequenceSegmentQueuedBuffer, self.sequenceSegmentQueuedData).wait()
        cl.enqueue_read_buffer(self.queue, self._synapsesCountBuffer, self.synapsesCountData).wait()
        cl.enqueue_read_buffer(self.queue, self._newSynapsesCountBuffer, self.newSynapsesCountData).wait()
        cl.enqueue_read_buffer(self.queue, self._synapsePermanenceBuffer, self.synapsePermanenceData).wait()
        cl.enqueue_read_buffer(self.queue, self._synapsePermanenceQueuedBuffer, self.synapsePermanenceQueuedData).wait()
        cl.enqueue_read_buffer(self.queue, self._synapseTargetColumnBuffer, self.synapseTargetColumnData).wait()
        cl.enqueue_read_buffer(self.queue, self._synapseTargetCellBuffer, self.synapseTargetCellData).wait()
        cl.enqueue_read_buffer(self.queue, self._isLearningCellInColumnBuffer, self.isLearningCellInColumnData).wait()
        cl.enqueue_read_buffer(self.queue, self._outputBuffer, self.outputData).wait()
        cl.enqueue_read_buffer(self.queue, self._settingsBuffer, self._settings).wait()

    def _prepare_kernel(self, columns, settings):
        exec_evt = self.program.initializeLayer(
            self.queue, (self._columns_per_layer,), (self._work_group_size,),
            self._settingsBuffer, self._cellStateBuffer,
            self._hasCellQueuedChangesBuffer, self._newSegmentQueuedCountBuffer,
            self._segmentsCountBuffer,
            self._isSegmentSequentialBuffer, self._hasSegmentQueuedChangesBuffer,
            self._sequenceSegmentQueuedBuffer, self._synapsesCountBuffer,
            self._newSynapsesCountBuffer,
            self._synapsePermanenceBuffer, self._synapsePermanenceQueuedBuffer,
            self._synapseTargetColumnBuffer, self._synapseTargetCellBuffer,
            self._isLearningCellInColumnBuffer, self._mwc64x_state_Buffer, self._randomValueBuffer)
        exec_evt.wait()

        if self._profile:
            elapsed = 1e-9 * (exec_evt.profile.end - exec_evt.profile.start)

            print("Execution time of initializeLayer: %g s" % elapsed)

    def execute_kernel(self, output_values_from_sp_):
        output_values_from_sp = np.array(output_values_from_sp_).astype(np.uint8)

        self._valuesFromSpBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR,
                                              hostbuf=output_values_from_sp)

        exec_evt = self.program.executeKernels(
            self.queue, (self._columns_per_layer,), (self._work_group_size,),
            self._settingsBuffer, self._cellStateBuffer,
            self._hasCellQueuedChangesBuffer, self._newSegmentQueuedCountBuffer,
            self._segmentsCountBuffer,
            self._isSegmentSequentialBuffer, self._hasSegmentQueuedChangesBuffer,
            self._sequenceSegmentQueuedBuffer, self._synapsesCountBuffer,
            self._newSynapsesCountBuffer,
            self._synapsePermanenceBuffer, self._synapsePermanenceQueuedBuffer,
            self._synapseTargetColumnBuffer, self._synapseTargetCellBuffer,
            self._isLearningCellInColumnBuffer, self._usedLearningCellsBuffer,
            self._valuesFromSpBuffer,
            self._outputBuffer, self._mwc64x_state_Buffer)
        exec_evt.wait()

        # if self._profile:
        #     elapsed = 1e-9 * (exec_evt.profile.end - exec_evt.profile.start)
        #
        #     print("Execution time of executeKernels: %g s" % elapsed)

        return self.outputData

    def _load_kernel(self, show_kernel=False):
        f = open(self.kernel_path + ".cl")

        kernels_dir = self._get_kernels_dir()

        fstr = f.read()
        f.close()
        f = open(os.path.join(kernels_dir, "additional_temporal_pooler_functions.cl"))
        fstr += f.read()
        f.close()
        if show_kernel:
            print fstr

        self.program = cl.Program(self.ctx, fstr). \
            build("-I " + kernels_dir + " " + self.build_defines)